;;;-----------------------------------------------------------------------------
;;; CLiCC: The Common Lisp to C Compiler
;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;-----------------------------------------------------------------------------
;;; CLiCC has been developed as part of the APPLY research project,
;;; funded by the German Ministry of Research and Technology.
;;; 
;;; CLiCC is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; CLiCC is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License in file COPYING for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;-----------------------------------------------------------------------------
;;; Funktion : Ein Interpretierer fuer Teile der Zwischensprache.
;;;
;;; $Revision: 1.71 $
;;; $Id: p1eval.lisp,v 1.71 1994/11/22 14:49:16 hk Exp $
;;;-----------------------------------------------------------------------------

(in-package "COM.INFORMATIMAGO.CLICC")

;;------------------------------------------------------------------------------
(defun p1-eval (form)
  (catch 'cant-eval
    (let ((v (zw-eval (in-compile-time-env (p1-form form)) (zw-empty-env))))
      (labels ((chk-no-fun (v)
                 (typecase v
                   (symbol nil)         ; CLtL1 (!) symbol < function
                   (cons (chk-no-fun (car v)) (chk-no-fun (cdr v)))
                   (function (unable-to-eval v))
                   #+(OR CLISP LUCID) (string nil)        
                   ((array standard-char) nil)
                   (array (map-array #'chk-no-fun v))
                   (t nil))))
        (chk-no-fun v)
        (values v t)))))

;;------------------------------------------------------------------------------
(defun unable-to-eval (form)
  (throw 'cant-eval
    (values form NIL)))

;;------------------------------------------------------------------------------
;; Funktion, die ein leeres Environment generiert.
;;------------------------------------------------------------------------------
(defun zw-empty-env () ())

;;------------------------------------------------------------------------------
;; erweitert das aktuelle Environment um eine Bindung fuer var mit dem initialen
;; Wert value.
;;------------------------------------------------------------------------------
(defmacro zw-bind (var value env)
  `(push (cons ,var ,value) ,env))

;;------------------------------------------------------------------------------
;; Setzt den Wert von var in env destruktiv auf value.
;;------------------------------------------------------------------------------
(defmacro zw-setq (var value env)
  `(setf (cdr (assoc ,var ,env)) ,value))

;;------------------------------------------------------------------------------
;; Bestimmt den aktuellen Wert von var in env.
;;------------------------------------------------------------------------------
(defmacro zw-get-bind (var env)
  `(cdr (assoc ,var ,env)))


;;------------------------------------------------------------------------------
;; Fehlerfall
;;------------------------------------------------------------------------------
(defmethod zw-eval ((anything t) env)
  (declare (ignore env))
  #+PCL (when (functionp anything)
          (return-from zw-eval anything))
  (error "can't eval ~A" anything))

;;------------------------------------------------------------------------------
;; Auswertung von Literalen
;;------------------------------------------------------------------------------


;;------------------------------------------------------------------------------
;; Die als strukturierte Literale repraesentierten Listen enthalten mit Ausnahme
;; von Symbolen und Strukturen Literale in ihrer Lisp-Repraesentation.
;; Der Interpretierer ruft in diesem Fall eval-structure-literal-list auf.
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form structured-literal) env)
  (let ((value (?value form)))
    (etypecase value
      ((or list array) (eval-structured-literal-value value env)))))

;;------------------------------------------------------------------------------
;; Die in der Liste enthaltenen Symbole werden ausgewertet, alle anderen
;; Literale bleiben erhalten.
;;------------------------------------------------------------------------------
(defun eval-structured-literal-value (x &optional env)
  (labels
      ((f (x)
         (typecase x
           (cons (cons (f (car x)) (f (cdr x))))
           (string x)
           (array (copy-array #'f x))
           (sym  (zw-eval x env))
           (literal-instance (zw-eval x env))
           (t x))))
    (f x)))

;;------------------------------------------------------------------------------
(defmethod zw-eval ((a-simple-literal simple-literal) env)
  (declare (ignore env))
  (?value a-simple-literal))

(defmethod zw-eval ((a-null-form null-form) env)
  (declare (ignore env))
  '())

;;------------------------------------------------------------------------------
;; literal-instance
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form literal-instance) env)
  (declare (ignore env))
  (unable-to-eval form))

;;------------------------------------------------------------------------------
;; Symbole
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form sym) env)
  (declare (ignore env))
  (?symbol form))

;;------------------------------------------------------------------------------
;; Klassen
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form class-def) env)
  (declare (ignore env))
  form)

;;------------------------------------------------------------------------------
;; Auswertung eines lesenden Zugriffs auf eine Variable
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form var-ref) env)
  (setq form (?var form))
  (cond
    ((local-static-p form)
     (zw-get-bind form env))
    (t (setq form (?sym form))
       (cond
         ((constant-value-p form) (zw-eval (?constant-value form) env))
         (t (unable-to-eval form))))))

;;------------------------------------------------------------------------------
;; Auswertung einer benannten Konstante
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form defined-named-const) env)
  (if (member (?value form) '(:unknown :forward))
      (unable-to-eval form)
      (zw-eval (?value form) env)))

(defmethod zw-eval ((form imported-named-const) env)
  (declare (ignore env))
  (unable-to-eval form))

;;------------------------------------------------------------------------------
;; Zuweisung an eine Variable
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form setq-form) env)
  (let ((var (?var (?location form))))
    (cond
      ((local-static-p var)
        (zw-setq var (zw-eval (?form form) env) env))
      (t (unable-to-eval form)))))

;;------------------------------------------------------------------------------
;; Hintereinanderausfuehrung
;;------------------------------------------------------------------------------
(defmethod zw-eval ((a-progn-form progn-form) env)
  (let ((evaluator #'(lambda (form)
                       (zw-eval form env))))
    (mapc-progn-form-list (?form-list a-progn-form) evaluator evaluator)))

;;------------------------------------------------------------------------------
;; Konditional
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form if-form) env)
  (if (zw-not-null-p (zw-eval (?pred form) env))
      (zw-eval (?then form) env)
      (zw-eval (?else form) env)))

;;------------------------------------------------------------------------------
;; Hinzufuegen neuer Variablenbindungen
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form let*-form) env)
  (mapc #'(lambda (variable init-form)
            (if (local-static-p variable)
                (zw-bind variable (zw-eval init-form env) env)
                (unable-to-eval form)))
        (?var-list form)
        (?init-list form))
  (zw-eval (?body form) env))

;;------------------------------------------------------------------------------
;; Multiple Values
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form mv-lambda) env)
  (multiple-value-call
      (zw-eval (make-defined-fun :symbol 'mv-lambda
                                 :params (?params form)
                                 :body (?body form))
               env)
    (zw-eval (?arg form) env)))
  
;;------------------------------------------------------------------------------
;; Tagbody und Go
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form tagbody-form) env)
  (let ((secret (gensym))
        (next-tagged-forms (?tagged-form-list form))
        target)
    (zw-bind form secret env)
    (setq target (catch secret (zw-eval (?first-form form) env)))
    (loop
     (if (tagged-form-p target)
         (setq next-tagged-forms (member target (?tagged-form-list form)))
         (unless next-tagged-forms (return)))
     (setq target
           (catch secret (zw-eval (?form (pop next-tagged-forms)) env))))))

;;------------------------------------------------------------------------------
(defmethod zw-eval ((form tagged-form) env)
  (throw (zw-get-bind (?tagbody form) env) form))

;------------------------------------------------------------------------------
;; Continuations
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form let/cc-form) env)
  (let ((secret (gensym)))
    (zw-bind (?cont form) #'(lambda (x) (throw secret x)) env)
    (catch secret
      (zw-eval (?body form) env))))

;;------------------------------------------------------------------------------
(defmethod zw-eval ((cont cont) env)
  (zw-get-bind cont env))

;;------------------------------------------------------------------------------
;; Funktionales Objekt
;; Globale und importierte Funktionen greifen nicht auf env zu
;;------------------------------------------------------------------------------
(defmethod zw-eval ((fun fun) env)
  #'(lambda (&rest arg-list)
      (zw-apply fun arg-list env)))

;;------------------------------------------------------------------------------
;; Funktion des Wirts Lispsystems.
;; Kommt nur im Rumpf von Makroexpansionsfunktionen vor und nur fuer Lisp
;; Systemfunktionen.
;;------------------------------------------------------------------------------
#-PCL                        ; siehe (zw-eval T T)
(defmethod zw-eval ((function function) env)
  (declare (ignore env))
  function)

;;------------------------------------------------------------------------------
;; Funktionsanwendung
;;------------------------------------------------------------------------------
(defmethod zw-eval ((app app) env)
  (apply (zw-eval (?form app) env)
         (mapcar #'(lambda (arg) (zw-eval arg env)) (?arg-list app))))

;;------------------------------------------------------------------------------
;; Definition lokaler Funktionen
;; Die Definitionen brauchen nicht betrachtet zu werden, da die angewandten
;; Vorkommen alle relevanten Informationen enthalten.
;;------------------------------------------------------------------------------
(defmethod zw-eval ((form labels-form) env)
  (zw-eval (?body form) env))

;;------------------------------------------------------------------------------
(defmethod zw-apply ((fun defined-fun) arg-list env)
  (let ((params (?params fun)))
    (dolist (var (?var-list params))
      (zw-bind var (pop arg-list) env))
    (dolist (opt (?opt-list params))
      (zw-bind (?var opt)
               (cond
                 (arg-list
                  (when (?suppl opt) (zw-bind (?suppl opt) (zw-t) env))
                  (pop arg-list))
                 (t (when (?suppl opt) (zw-bind (?suppl opt) nil env))
                    (zw-eval (?init opt) env)))
               env))
    (when (?rest params)
      (zw-bind (?rest params) arg-list env))
    (when (?key-list params)
      (unable-to-eval (?symbol fun))))
  (zw-eval (?body fun) env))

;;------------------------------------------------------------------------------
(defmethod zw-apply ((fun imported-fun) arg-list env)
  (let ((app-fun (zw-symbol-fun (?symbol fun))))
    (cond
      (app-fun (apply app-fun arg-list))
      ((slot-boundp fun 'syntax) (zw-apply (?syntax fun) arg-list env))
      (T (unable-to-eval (?symbol fun))))))

;;------------------------------------------------------------------------------
(defun zw-t ()
  (get-symbol-bind t))
  
;;------------------------------------------------------------------------------
(defun zw-not-null-p (x) x)

;;------------------------------------------------------------------------------
(defun zw-error (&rest args) (apply #'clicc-error args))

;;------------------------------------------------------------------------------
;; Unser eigenes symbol-function
;;------------------------------------------------------------------------------
(defun zw-symbol-fun (fun-sym)
  (gethash fun-sym *zw-sym-fun-hash-table*))

;;------------------------------------------------------------------------------
;; Initialisierung der Hash-Table fuer zw-symbol-fun
;;------------------------------------------------------------------------------
(defun init-zw-sym-fun-hash-table ()
  (setq *zw-sym-fun-hash-table* (make-hash-table :test #'equal :size 377))
  (macrolet ((sym-fun (name fun)
               `(setf (gethash ',name *zw-sym-fun-hash-table*) #',fun)))
    ;; #-CLISP
    ;; #+CLISP
    ;; ((sym-fun (name fun)
    ;;           `(let (dummy-to-avoid-error)
    ;;             (setq dummy-to-avoid-error #',fun)
    ;;             (setf (gethash ',name *zw-sym-fun-hash-table*)
    ;;              dummy-to-avoid-error))))
    
    ;; Predicates
    ;; typep und subtypep des Wirts-Lispsystems nicht benutzen
    (sym-fun com.informatimago.clicc.lisp::null null)
    (sym-fun com.informatimago.clicc.lisp::symbolp symbolp)
    (sym-fun com.informatimago.clicc.lisp::atom atom)
    (sym-fun com.informatimago.clicc.lisp::consp consp)
    (sym-fun com.informatimago.clicc.lisp::listp listp)
    (sym-fun com.informatimago.clicc.lisp::numberp numberp)
    (sym-fun com.informatimago.clicc.lisp::integerp integerp)
    (sym-fun com.informatimago.clicc.lisp::rationalp rationalp)
    (sym-fun com.informatimago.clicc.lisp::floatp floatp)
    ;; .. real, complex
    (sym-fun com.informatimago.clicc.lisp::characterp characterp)
    (sym-fun com.informatimago.clicc.lisp::stringp stringp)
    ;; bit-vector-p, vectorp, simple-vector-p
    (sym-fun com.informatimago.clicc.lisp::simple-string-p simple-string-p)
    ;; simple-bit-vector-p, arrayp, packagep, functionp, compiled-function-p
    ;; common
    (sym-fun com.informatimago.clicc.lisp::eq eq)
    (sym-fun com.informatimago.clicc.lisp::eql eql)
    (sym-fun com.informatimago.clicc.lisp::equal equal)
    (sym-fun com.informatimago.clicc.lisp::not not)

    ;; Control Structure
    ;; kein symbol-value, symbol-function, boundp, fboundp, special-form-p, set
    ;; kein makunbound, fmakunbound
    ;; kein get-setf-method, get-setf-method-multiple-value
    (sym-fun
     com.informatimago.clicc.lisp::apply
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'apply fn args)
           (error "apply: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::funcall
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply fn args)
           (error "funcall: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::mapcar
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'mapcar fn args)
           (error "mapcar: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::maplist
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'maplist fn args)
           (error "maplist: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::mapc
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'mapc fn args)
           (error "mapc: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::mapl
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'mapl fn args)
           (error "mapl: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::mapcan
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'mapcan fn args)
           (error "mapcan: first argument ~a must be a function" fn))))
    (sym-fun
     com.informatimago.clicc.lisp::mapcon
     (lambda (fn &rest args)
       (if (functionp fn)
           (apply #'mapcon fn args)
           (error "mapcon: first argument ~a must be a function" fn))))
    (sym-fun com.informatimago.clicc.lisp::values values)

    ;; Symbols
    ;; keine P-Listen von Symbolen bearbeiten !
    (sym-fun com.informatimago.clicc.lisp::getf getf)
    (sym-fun
     com.informatimago.clicc.rt::set-prop
     (lambda (plist indicator value)
       (labels ((get-prop (list indicator)
                  (cond
                    ((atom list) nil) 
                    ((eq (car list) indicator) list)
                    (t (get-prop (cddr list) indicator)))))
         (let ((list (get-prop plist indicator)))
           (cond
             (list
              (rplaca (cdr list) value)
              plist)
             (t (cons indicator (cons value plist))))))))
    
    (sym-fun
     com.informatimago.clicc.rt::remf-internal
     (lambda (list indicator)
       (cond
         ((atom list) (values nil nil))
         ((eq (car list) indicator) (values (cddr list) t))
         (t (do ((list1 list (cddr list1))
                 (list2 (cddr list) (cddr list2)))
                ((atom list2) (values nil nil)) ;end test
              (when (eq (car list2) indicator)
                (rplacd (cdr list1) (cddr list2))
                (return (values list t))))))))

    (sym-fun com.informatimago.clicc.lisp::get-properties get-properties)
    (sym-fun com.informatimago.clicc.lisp::symbol-name symbol-name)
    (sym-fun com.informatimago.clicc.lisp::make-symbol make-symbol)
    (sym-fun com.informatimago.clicc.lisp::copy-symbol copy-symbol)
    (sym-fun com.informatimago.clicc.lisp::gensym gensym)
    (sym-fun com.informatimago.clicc.lisp::gentemp gentemp)
    (sym-fun com.informatimago.clicc.lisp::symbol-package symbol-package)
    (sym-fun com.informatimago.clicc.lisp::keywordp keywordp)

    ;; Packages
    (sym-fun com.informatimago.clicc.lisp::intern intern)
    (sym-fun com.informatimago.clicc.lisp::find-package find-package)

    ;; Numbers
    (sym-fun com.informatimago.clicc.lisp::zerop zerop)
    (sym-fun com.informatimago.clicc.lisp::plusp plusp)
    (sym-fun com.informatimago.clicc.lisp::minusp minusp)
    (sym-fun com.informatimago.clicc.lisp::oddp oddp)
    (sym-fun com.informatimago.clicc.lisp::evenp evenp)
    (sym-fun com.informatimago.clicc.lisp::= =)
    (sym-fun com.informatimago.clicc.lisp::/= /=)
    (sym-fun com.informatimago.clicc.lisp::< <)
    (sym-fun com.informatimago.clicc.lisp::> >)
    (sym-fun com.informatimago.clicc.lisp::<= <=)
    (sym-fun com.informatimago.clicc.lisp::>= >=)
    (sym-fun com.informatimago.clicc.lisp::max max)
    (sym-fun com.informatimago.clicc.lisp::min min)
    (sym-fun com.informatimago.clicc.lisp::+ +)
    (sym-fun com.informatimago.clicc.lisp::- -)
    (sym-fun com.informatimago.clicc.lisp::* *)
    (sym-fun com.informatimago.clicc.lisp::/ /)
    (sym-fun com.informatimago.clicc.lisp::1+ 1+)
    (sym-fun com.informatimago.clicc.lisp::1- 1-)
    (sym-fun com.informatimago.clicc.lisp::exp exp)
    (sym-fun com.informatimago.clicc.lisp::expt expt)
    (sym-fun com.informatimago.clicc.lisp::log log)
    (sym-fun com.informatimago.clicc.lisp::sqrt sqrt)
    (sym-fun com.informatimago.clicc.lisp::isqrt isqrt)
    (sym-fun com.informatimago.clicc.lisp::abs abs)
    (sym-fun com.informatimago.clicc.lisp::signum signum)
    (sym-fun com.informatimago.clicc.lisp::float float)
    (sym-fun com.informatimago.clicc.lisp::floor floor)
    (sym-fun com.informatimago.clicc.lisp::ceiling ceiling)
    (sym-fun com.informatimago.clicc.lisp::truncate truncate)
    (sym-fun com.informatimago.clicc.lisp::round round)
    (sym-fun com.informatimago.clicc.lisp::mod mod)
    (sym-fun com.informatimago.clicc.lisp::rem rem)
    (sym-fun com.informatimago.clicc.lisp::logior logior)
    (sym-fun com.informatimago.clicc.lisp::logxor logxor)
    (sym-fun com.informatimago.clicc.lisp::logand logand)
    (sym-fun com.informatimago.clicc.lisp::logeqv logeqv)
    (sym-fun com.informatimago.clicc.lisp::lognand lognand)
    (sym-fun com.informatimago.clicc.lisp::lognor lognor)
    (sym-fun com.informatimago.clicc.lisp::logandc1 logandc1)
    (sym-fun com.informatimago.clicc.lisp::logandc2 logandc2)
    (sym-fun com.informatimago.clicc.lisp::logorc1 logorc1)
    (sym-fun com.informatimago.clicc.lisp::logorc2 logorc2)
    (sym-fun com.informatimago.clicc.lisp::ash ash)

    ;; Characters
    (sym-fun com.informatimago.clicc.lisp::standard-char-p standard-char-p)
    (sym-fun com.informatimago.clicc.lisp::graphic-char-p graphic-char-p)
    (sym-fun com.informatimago.clicc.lisp::alpha-char-p alpha-char-p)
    (sym-fun com.informatimago.clicc.lisp::upper-case-p upper-case-p)
    (sym-fun com.informatimago.clicc.lisp::lower-case-p lower-case-p)
    (sym-fun com.informatimago.clicc.lisp::both-case-p both-case-p)
    (sym-fun com.informatimago.clicc.lisp::digit-char-p digit-char-p)
    (sym-fun com.informatimago.clicc.lisp::alphanumericp alphanumericp)
    (sym-fun com.informatimago.clicc.lisp::char= char=)
    (sym-fun com.informatimago.clicc.lisp::char\= char\=)
    (sym-fun com.informatimago.clicc.lisp::char< char<)
    (sym-fun com.informatimago.clicc.lisp::char> char>)
    (sym-fun com.informatimago.clicc.lisp::char<= char<=)
    (sym-fun com.informatimago.clicc.lisp::char>= char>=)
    (sym-fun com.informatimago.clicc.lisp::char-equal char-equal)
    (sym-fun com.informatimago.clicc.lisp::char-not-equal char-not-equal)
    (sym-fun com.informatimago.clicc.lisp::char-lessp char-lessp)
    (sym-fun com.informatimago.clicc.lisp::char-greaterp char-greaterp)
    (sym-fun com.informatimago.clicc.lisp::char-not-greaterp char-not-greaterp)
    (sym-fun com.informatimago.clicc.lisp::char-not-lessp char-not-lessp)
    (sym-fun com.informatimago.clicc.lisp::char-code char-code)
    (sym-fun com.informatimago.clicc.lisp::code-char code-char)
    (sym-fun com.informatimago.clicc.lisp::character character)
    (sym-fun com.informatimago.clicc.lisp::char-upcase char-upcase)
    (sym-fun com.informatimago.clicc.lisp::char-downcase char-downcase)
    (sym-fun com.informatimago.clicc.lisp::digit-char digit-char)
    (sym-fun com.informatimago.clicc.lisp::char-name char-name)
    (sym-fun com.informatimago.clicc.lisp::name-char name-char)
    
    ;; Sequences
    (sym-fun com.informatimago.clicc.lisp::elt elt)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::elt) (lambda (v s i) (setf (elt s i) v)))
    (sym-fun com.informatimago.clicc.lisp::subseq subseq)
    (sym-fun com.informatimago.clicc.lisp::copy-seq copy-seq)
    (sym-fun com.informatimago.clicc.lisp::length length)
    (sym-fun com.informatimago.clicc.lisp::reverse reverse)
    (sym-fun com.informatimago.clicc.lisp::nreverse nreverse)
    (sym-fun com.informatimago.clicc.lisp::make-sequence make-sequence)
    (sym-fun com.informatimago.clicc.lisp::concatenate concatenate)
    (sym-fun com.informatimago.clicc.lisp::map map)
    (sym-fun com.informatimago.clicc.lisp::some some)
    (sym-fun com.informatimago.clicc.lisp::every every)
    (sym-fun com.informatimago.clicc.lisp::notany notany)
    (sym-fun com.informatimago.clicc.lisp::notevery notevery)
    (sym-fun com.informatimago.clicc.lisp::reduce reduce)
    (sym-fun com.informatimago.clicc.lisp::fill fill)
    (sym-fun com.informatimago.clicc.lisp::replace replace)
    (sym-fun com.informatimago.clicc.lisp::remove remove)
    (sym-fun com.informatimago.clicc.lisp::remove-if remove-if)
    (sym-fun com.informatimago.clicc.lisp::remove-if-not remove-if-not)
    (sym-fun com.informatimago.clicc.lisp::delete delete)
    (sym-fun com.informatimago.clicc.lisp::delete-if delete-if)
    (sym-fun com.informatimago.clicc.lisp::delete-if-not delete-if-not)
    (sym-fun com.informatimago.clicc.lisp::remove-duplicates remove-duplicates)
    (sym-fun com.informatimago.clicc.lisp::delete-duplicates delete-duplicates)
    (sym-fun com.informatimago.clicc.lisp::substitute substitute)
    (sym-fun com.informatimago.clicc.lisp::substitute-if substitute-if)
    (sym-fun com.informatimago.clicc.lisp::substitute-if-not substitute-if-not)
    (sym-fun com.informatimago.clicc.lisp::nsubstitute nsubstitute)
    (sym-fun com.informatimago.clicc.lisp::nsubstitute-if nsubstitute-if)
    (sym-fun com.informatimago.clicc.lisp::nsubstitute-if-not nsubstitute-if-not)
    (sym-fun com.informatimago.clicc.lisp::find find)
    (sym-fun com.informatimago.clicc.lisp::find-if find-if)
    (sym-fun com.informatimago.clicc.lisp::find-if-not find-if-not)
    (sym-fun com.informatimago.clicc.lisp::position position)
    (sym-fun com.informatimago.clicc.lisp::position-if position-if)
    (sym-fun com.informatimago.clicc.lisp::position-if-not position-if-not)
    (sym-fun com.informatimago.clicc.lisp::count count)
    (sym-fun com.informatimago.clicc.lisp::count-if count-if)
    (sym-fun com.informatimago.clicc.lisp::count-if-not count-if-not)
    (sym-fun com.informatimago.clicc.lisp::mismatch mismatch)
    (sym-fun com.informatimago.clicc.lisp::search search)
    (sym-fun com.informatimago.clicc.lisp::sort sort)
    (sym-fun com.informatimago.clicc.lisp::stable-sort stable-sort)
    (sym-fun com.informatimago.clicc.lisp::merge merge)

    ;; Lists
    (sym-fun com.informatimago.clicc.lisp::car car)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::car) (lambda (v c) (setf (car c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdr cdr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdr) (lambda (v c) (setf (cdr c) v)))
    (sym-fun com.informatimago.clicc.lisp::caar caar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caar) (lambda (v c) (setf (caar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadr cadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadr) (lambda (v c) (setf (cadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdar cdar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdar) (lambda (v c) (setf (cdar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cddr cddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cddr) (lambda (v c) (setf (cddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::caaar caaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caaar) (lambda (v c) (setf (caaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::caadr caadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caadr) (lambda (v c) (setf (caadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadar cadar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadar) (lambda (v c) (setf (cadar c) v)))
    (sym-fun com.informatimago.clicc.lisp::caddr caddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caddr) (lambda (v c) (setf (caddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadar cadar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadar) (lambda (v c) (setf (cadar c) v)))
    (sym-fun com.informatimago.clicc.lisp::caddr caddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caddr) (lambda (v c) (setf (caddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdaar cdaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdaar) (lambda (v c) (setf (cdaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdadr cdadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdadr) (lambda (v c) (setf (cdadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cddar cddar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cddar) (lambda (v c) (setf (cddar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdddr cdddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdddr) (lambda (v c) (setf (cdddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::caaaar caaaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caaaar) (lambda (v c) (setf (caaaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::caaadr caaadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caaadr) (lambda (v c) (setf (caaadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::caadar caadar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caadar) (lambda (v c) (setf (caadar c) v)))
    (sym-fun com.informatimago.clicc.lisp::caaddr caaddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caaddr) (lambda (v c) (setf (caaddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadaar cadaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadaar) (lambda (v c) (setf (cadaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadadr cadadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadadr) (lambda (v c) (setf (cadadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::caddar caddar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::caddar) (lambda (v c) (setf (caddar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cadddr cadddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cadddr) (lambda (v c) (setf (cadddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdaaar cdaaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdaaar) (lambda (v c) (setf (cdaaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdaadr cdaadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdaadr) (lambda (v c) (setf (cdaadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdadar cdadar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdadar) (lambda (v c) (setf (cdadar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdaddr cdaddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdaddr) (lambda (v c) (setf (cdaddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cddaar cddaar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cddaar) (lambda (v c) (setf (cddaar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cddadr cddadr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cddadr) (lambda (v c) (setf (cddadr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cdddar cdddar)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cdddar) (lambda (v c) (setf (cdddar c) v)))
    (sym-fun com.informatimago.clicc.lisp::cddddr cddddr)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::cddddr) (lambda (v c) (setf (cddddr c) v)))
    (sym-fun com.informatimago.clicc.lisp::cons cons)
    (sym-fun com.informatimago.clicc.lisp::tree-equal tree-equal)
    (sym-fun com.informatimago.clicc.lisp::endp endp)
    (sym-fun com.informatimago.clicc.lisp::list-length list-length)
    (sym-fun com.informatimago.clicc.lisp::nth nth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::nth) (lambda (v l i) (setf (nth l i) v)))
    (sym-fun com.informatimago.clicc.lisp::first first)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::first) (lambda (v c) (setf (first c) v)))
    (sym-fun com.informatimago.clicc.lisp::second second)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::second) (lambda (v c) (setf (second c) v)))
    (sym-fun com.informatimago.clicc.lisp::third third)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::third) (lambda (v c) (setf (third c) v)))
    (sym-fun com.informatimago.clicc.lisp::fourth fourth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::fourth) (lambda (v c) (setf (fourth c) v)))
    (sym-fun com.informatimago.clicc.lisp::fifth fifth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::fifth) (lambda (v c) (setf (fifth c) v)))
    (sym-fun com.informatimago.clicc.lisp::sixth sixth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::sixth) (lambda (v c) (setf (sixth c) v)))
    (sym-fun com.informatimago.clicc.lisp::seventh seventh)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::seventh) (lambda (v c) (setf (seventh c) v)))
    (sym-fun com.informatimago.clicc.lisp::eighth eighth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::eighth) (lambda (v c) (setf (eighth c) v)))
    (sym-fun com.informatimago.clicc.lisp::ninth ninth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::ninth) (lambda (v c) (setf (ninth c) v)))
    (sym-fun com.informatimago.clicc.lisp::tenth tenth)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::tenth) (lambda (v c) (setf (tenth c) v)))
    (sym-fun com.informatimago.clicc.lisp::rest rest)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::rest) (lambda (v c) (setf (rest c) v)))
    (sym-fun com.informatimago.clicc.lisp::nthcdr nthcdr)
    (sym-fun com.informatimago.clicc.lisp::last last)
    (sym-fun com.informatimago.clicc.lisp::list list)
    (sym-fun com.informatimago.clicc.lisp::list* list*)
    (sym-fun com.informatimago.clicc.lisp::make-list make-list)
    (sym-fun com.informatimago.clicc.lisp::append append)
    (sym-fun com.informatimago.clicc.lisp::copy-list copy-list)
    (sym-fun com.informatimago.clicc.lisp::copy-alist copy-alist)
    (sym-fun com.informatimago.clicc.lisp::copy-tree copy-tree)
    (sym-fun com.informatimago.clicc.lisp::revappend revappend)
    (sym-fun com.informatimago.clicc.lisp::nconc nconc)
    (sym-fun com.informatimago.clicc.lisp::nreconc nreconc)
    (sym-fun com.informatimago.clicc.lisp::butlast butlast)
    (sym-fun com.informatimago.clicc.lisp::nbutlast nbutlast)
    (sym-fun com.informatimago.clicc.lisp::ldiff ldiff)
    (sym-fun com.informatimago.clicc.lisp::rplaca rplaca)
    (sym-fun com.informatimago.clicc.lisp::rplacd rplacd)
    (sym-fun com.informatimago.clicc.lisp::subst subst)
    (sym-fun com.informatimago.clicc.lisp::subst-if subst-if)
    (sym-fun com.informatimago.clicc.lisp::subst-if-not subst-if-not)
    (sym-fun com.informatimago.clicc.lisp::nsubst nsubst)
    (sym-fun com.informatimago.clicc.lisp::nsubst-if nsubst-if)
    (sym-fun com.informatimago.clicc.lisp::nsubst-if-not nsubst-if-not)
    (sym-fun com.informatimago.clicc.lisp::sublis sublis)
    (sym-fun com.informatimago.clicc.lisp::nsublis nsublis)
    (sym-fun com.informatimago.clicc.lisp::member member)
    (sym-fun com.informatimago.clicc.lisp::member-if member-if)
    (sym-fun com.informatimago.clicc.lisp::member-if-not member-if-not)
    (sym-fun com.informatimago.clicc.lisp::tailp tailp)
    (sym-fun com.informatimago.clicc.lisp::adjoin adjoin)
    (sym-fun com.informatimago.clicc.lisp::union union)
    (sym-fun com.informatimago.clicc.lisp::nunion nunion)
    (sym-fun com.informatimago.clicc.lisp::intersection intersection)
    (sym-fun com.informatimago.clicc.lisp::set-difference set-difference)
    (sym-fun com.informatimago.clicc.lisp::subsetp subsetp)
    (sym-fun com.informatimago.clicc.lisp::acons acons)
    (sym-fun com.informatimago.clicc.lisp::pairlis pairlis)
    (sym-fun com.informatimago.clicc.lisp::assoc assoc)
    (sym-fun com.informatimago.clicc.lisp::assoc-if assoc-if)
    (sym-fun com.informatimago.clicc.lisp::assoc-if-not assoc-if-not)
    (sym-fun com.informatimago.clicc.lisp::rassoc rassoc)
    (sym-fun com.informatimago.clicc.lisp::rassoc-if rassoc-if)
    (sym-fun com.informatimago.clicc.lisp::rassoc-if-not rassoc-if-not)

    ;; Arrays
    ;; ..
    (sym-fun com.informatimago.clicc.lisp::aref aref)
    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::aref) (lambda (v &rest l) (setf (apply #'aref l) v)))
    (sym-fun com.informatimago.clicc.lisp::svref svref)

;;; is not required to be supported, in ANSI-CL use #'(setf svref) instead    
;;;    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::svref) (lambda (v l)
;;;                                  (setf (apply #'svref l) v)))
    ;; ..
    
    ;; Strings
    (sym-fun com.informatimago.clicc.lisp::char char)
;;;    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::char) (lambda (v l)
;;;                                 (setf (apply #'char l) v)))
    (sym-fun com.informatimago.clicc.lisp::schar schar)
;;;    (sym-fun (com.informatimago.clicc.lisp::setf com.informatimago.clicc.lisp::schar) (lambda (v l)
;;;                                  (setf (apply #'schar l) v)))
    (sym-fun com.informatimago.clicc.lisp::string= string=)
    (sym-fun com.informatimago.clicc.lisp::string-equal string-equal)
    (sym-fun com.informatimago.clicc.lisp::string< string<)
    (sym-fun com.informatimago.clicc.lisp::string> string>)
    (sym-fun com.informatimago.clicc.lisp::string<= string<=)
    (sym-fun com.informatimago.clicc.lisp::string>= string>=)
    (sym-fun com.informatimago.clicc.lisp::string/= string/=)
    (sym-fun com.informatimago.clicc.lisp::string-lessp string-lessp)
    (sym-fun com.informatimago.clicc.lisp::string-greaterp string-greaterp)
    (sym-fun com.informatimago.clicc.lisp::string-not-greaterp string-not-greaterp)
    (sym-fun com.informatimago.clicc.lisp::string-not-lessp string-not-lessp)
    (sym-fun com.informatimago.clicc.lisp::string-not-equal string-not-equal)
    (sym-fun com.informatimago.clicc.lisp::make-string make-string)
    (sym-fun com.informatimago.clicc.lisp::string-trim string-trim)
    (sym-fun com.informatimago.clicc.lisp::string-left-trim string-left-trim)
    (sym-fun com.informatimago.clicc.lisp::string-right-trim string-right-trim)
    (sym-fun com.informatimago.clicc.lisp::string-upcase string-upcase)
    (sym-fun com.informatimago.clicc.lisp::string-downcase string-downcase)
    (sym-fun com.informatimago.clicc.lisp::string-capitalize string-capitalize)
    (sym-fun com.informatimago.clicc.lisp::nstring-upcase nstring-upcase)
    (sym-fun com.informatimago.clicc.lisp::nstring-downcase nstring-downcase)
    (sym-fun com.informatimago.clicc.lisp::nstring-capitalize nstring-capitalize)
    (sym-fun com.informatimago.clicc.lisp::string string)

    ;; I/O
    ;; ..
    (sym-fun com.informatimago.clicc.lisp::write-to-string write-to-string)
    (sym-fun com.informatimago.clicc.lisp::prin1-to-string prin1-to-string)
    (sym-fun com.informatimago.clicc.lisp::princ-to-string princ-to-string)
    ;; ..
    (sym-fun com.informatimago.clicc.lisp::format format)

    ;; Errors
    (sym-fun com.informatimago.clicc.lisp::error error)
    (sym-fun com.informatimago.clicc.lisp::warn warn)
    (sym-fun com.informatimago.clicc.lisp::break break)
    ;; ..
    ))

;;------------------------------------------------------------------------------
;;;; THE END ;;;;
