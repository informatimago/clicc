;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;-----------------------------------------------------------------------------
;;;; CLiCC: The Common Lisp to C Compiler
;;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;;-----------------------------------------------------------------------------
;;;; CLiCC has been developed as part of the APPLY research project,
;;;; funded by the German Ministry of Research and Technology.
;;;; 
;;;; CLiCC is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; CLiCC is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License in file COPYING for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;;-----------------------------------------------------------------------------
;;;; Funktion : Startmodul zum Laden des Compilers in ein Common-LISP System
;;;;-----------------------------------------------------------------------------

(asdf:defsystem "com.informatimago.clicc.compiler"
  :description "The CLiCC compiler."
  :author "Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen"
  :version "0.6.5"
  :license "GPL2"
  :depends-on ()
  :components (
               (:file "packages")
               
               (:file "bq-read"          :depends-on ("packages"))
               (:file "clcmisc"          :depends-on ("packages"))
               (:file "clcdef"           :depends-on ("packages" "clcmisc"))
               (:file "config"           :depends-on ("packages"))
               
               (:file "appfuns"          :depends-on ("packages" "zsdef" "ffzsdef"))
               (:file "closure-analysis" :depends-on ("packages"))
               (:file "cgblock"          :depends-on ("packages"))
               (:file "cgcode"           :depends-on ("packages"))
               (:file "cgconst"          :depends-on ("packages"))
               (:file "cgdefs"           :depends-on ("packages"))
               (:file "cgforeign"        :depends-on ("packages"))
               (:file "cgfuns"           :depends-on ("packages"))
               (:file "cgif"             :depends-on ("packages"))
               (:file "cginline"         :depends-on ("packages" "p0init"))
               (:file "cgmain"           :depends-on ("packages" "cgdefs" "cgcode" "cgconst" "cgif" "cgblock" "cgvalues" "cginline" "cgvars" "cgfuns" "cgforeign"))
               (:file "cgvalues"         :depends-on ("packages"))
               (:file "cgvars"           :depends-on ("packages"))
               (:file "deffile"          :depends-on ("packages" "inline"))
               (:file "delete"           :depends-on ("packages" "traverse"))
               (:file "ffload"           :depends-on ("packages"))
               (:file "fftypes"          :depends-on ("packages"))
               (:file "ffzsdef"          :depends-on ("packages"))
               (:file "inline"           :depends-on ("packages" "weight" "p3main"))
               (:file "optimain"         :depends-on ("packages" "optimize" "subst" "tomain" "simplifier" "seomain" "weight"))
               (:file "optimize"         :depends-on ("packages"))
               (:file "p0init"           :depends-on ("packages"))
               (:file "p1decls"          :depends-on ("packages"))
               (:file "p1env"            :depends-on ("packages"))
               (:file "p1eval"           :depends-on ("packages"))
               (:file "p1generic"        :depends-on ("packages"))
               (:file "p1class"          :depends-on ("packages"))
               (:file "p1foreign"        :depends-on ("packages" "fftypes"))
               (:file "p1lambda"         :depends-on ("packages"))
               (:file "p1macexp"         :depends-on ("packages"))        
               (:file "p1macro"          :depends-on ("packages" "p1macexp" "p1setf"))
               (:file "p1main"           :depends-on ("packages" "p1macro" "p1foreign" "ffload" "p1tlf" "p1lambda" "p1spform" "p1type" "p1decls" "p1struct" "p1eval" "p1generic" "p1class" "sexport"))
               (:file "p1setf"           :depends-on ("packages"))
               (:file "p1struct"         :depends-on ("packages"))
               (:file "p1tlf"            :depends-on ("packages"))
               (:file "p1type"           :depends-on ("packages"))
               (:file "p3main"           :depends-on ("packages"))
               (:file "se-init"          :depends-on ("packages"))
               (:file "seomain"          :depends-on ("packages" "titypes"))
               (:file "sexport"          :depends-on ("packages" "traverse"))
               (:file "simplifier"       :depends-on ("packages" "delete" "titypes" "p1env"))
               (:file "static-effect"    :depends-on ("packages" "se-init" "optimize" "tail" "closure-analysis"))
               (:file "statistics"       :depends-on ("packages" "titypes" "traverse"))
               (:file "strconst"         :depends-on ("packages"))
               (:file "subst"            :depends-on ("packages" "delete"))
               (:file "tail"             :depends-on ("packages"))
               (:file "tiassert"         :depends-on ("packages" "titypes" "tidef" "timisc"))
               (:file "tidecl"           :depends-on ("packages" "titypes" "tidef" "timisc"))
               (:file "tidef"            :depends-on ("packages"))
               (:file "tiimpdec"         :depends-on ("packages" "titypes" "tidef" "timisc"))
               (:file "timain"           :depends-on ("packages" "tidef" "timisc" "tidecl" "tiimpdec" "tiassert" "tipass1" "tipass2" "tipass3" "statistics"))
               (:file "timisc"           :depends-on ("packages" "titypes" "tidef"))
               (:file "tipass1"          :depends-on ("packages" "titypes" "tidef" "traverse"))
               (:file "tipass2"          :depends-on ("packages" "titypes" "tidef" "tidecl" "timisc"))
               (:file "tipass3"          :depends-on ("packages" "titypes" "tidef" "traverse"))
               (:file "titypes"          :depends-on ("packages"))
               (:file "tomain"           :depends-on ("packages"))
               (:file "p1spform"         :depends-on ("packages"))
               (:file "traverse"         :depends-on ("packages"))
               (:file "weight"           :depends-on ("packages" "cgdefs" "cgcode" "cgvalues" "p3main"))
               (:file "zsdef"            :depends-on ("packages" "clcmisc" "clcdef" "titypes"))
               (:file "zsops"            :depends-on ("packages"))
               
               (:file "clcmain"          :depends-on ("packages" "strconst" "clcdef" "zsdef" "ffzsdef" "zsops" "clcmisc" "config" "appfuns" "p1env" "p0init" "p1main" "traverse" "inline" "delete" "static-effect" "p3main" "timain" "titypes" "optimain" "cgmain" "deffile"))
               (:file "clicc"            :depends-on ("packages" "clcmain"))
               
               )
  #+asdf-unicode :encoding #+asdf-unicode :utf-8)

