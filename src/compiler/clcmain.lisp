;;;-----------------------------------------------------------------------------
;;; CLiCC: The Common Lisp to C Compiler
;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;-----------------------------------------------------------------------------
;;; CLiCC has been developed as part of the APPLY research project,
;;; funded by the German Ministry of Research and Technology.
;;; 
;;; CLiCC is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; CLiCC is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License in file COPYING for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;-----------------------------------------------------------------------------
;;; Funktion : Definition der Function do-clicc zum Aufruf von CLiCC
;;;            Laden der uebrigen Module
;;;
;;; $Revision: 1.58 $
;;; $Id: clcmain.lisp,v 1.58 1994/12/06 15:46:10 hk Exp $
;;;-----------------------------------------------------------------------------

(in-package "COM.INFORMATIMAGO.CLICC")

;;------------------------------------------------------------------------------
;; Die restlichen Definitionen dienen zur leichteren Handhabung der Uebersetzung
;; von clicc bzw. des Clicc-Laufzeitsystems
;;------------------------------------------------------------------------------
(defvar *CLICC-LISP-DIR*)
(defvar *CLICC-LISP2C-DIR*)
(defvar *CLICC-COMPILER-DIR*)

(defparameter *lisp-extension* "lisp")
(defparameter *compiled-extension*   (pathname-type(compile-file-pathname #P"test.lisp")))


;;------------------------------------------------------------------------------
;; CLiCC Directories werden gesetzt.
;; Dies sind die Verzeichnisse des Laufzeitsystems und den
;; Quellcode des Compilers.
;; Die Wurzel des CLiCC-Filebaums und die Stellen, wo lisp.def/syntax und
;; sys.def/syntax zu finden sind, werden in config.lisp in der Funktion
;; setup-clicc-pathenames gesetzt.
;;------------------------------------------------------------------------------
(defun setup-clcload-pathnames ()
  (setq *CLICC-LISP-DIR*     (append *CLICC-DIR* (list "src" "runtime" "lisp")))
  (setq *CLICC-LISP2C-DIR*   (append *CLICC-DIR* (list "src" "runtime" "lisp2c")))
  (setq *CLICC-COMPILER-DIR* (append *CLICC-DIR* (list "src" "compiler"))))

(setup-clcload-pathnames)


;;------------------------------------------------------------------------------
;; DIE Funktion zum Aufruf von CLICC
;; Resultat zeigt an, ob die Uebersetzung erfolgreich war
;;------------------------------------------------------------------------------
(defun clicc (filename &KEY
                       (out              nil)
                         
                       (module-compiler  nil)
                       (lisp-module      nil)
                       (inline-module    nil)
                         
                       (verbose          *SHOW-VERSION*)
                       (print            *CLICC-PRINT*)
                       (memsizes         *MEMSIZES*)
                       (no-codegen       *NO-CODEGEN*)
                       (ti-level         *TI-Level*)
                       (obrep            *OBREP*)
                       (max-lines        *C-max-line-count*)
                       (split            *SPLIT-FILES*)
                       (flat-ifs         *FLAT-IFS*))

  (setq *FILENAME*         filename)
  (setq *OUT-FILENAME*     out)
  
  (setq *MODULE-COMPILER*  module-compiler)
  (setq *LISP-MODULE*      lisp-module)
  (setq *INLINE-MODULE*    inline-module)

  ;; Durch die Zuweisung werden die aktuellen Einstellungen zum Defaultwert
  ;; f�r den n�chsten Aufruf
  ;;------------------------
  (setq *SHOW-VERSION*     verbose)
  (setq *CLICC-PRINT*      print)
  (setq *MEMSIZES*         memsizes)
  (setq *NO-CODEGEN*       no-codegen)
  (setq *TI-Level*         ti-level)
  (setq *OBREP*            obrep)
  (setq *C-max-line-count* max-lines)
  (setq *SPLIT-FILES*      split)
  (setq *FLAT-IFS*         flat-ifs)
  
  ;; Package automatisch auf CLICC setzen, damit Debugging erleichtert wird
  ;;-----------------------------------------------------------------------
  (setf *package* (find-package *clicc-package-name*))
  
  (let* ((lisp-package (find-package *lisp-package-name*))
         (lisp-name    (package-name lisp-package))
         (lisp-nick    (package-nicknames lisp-package))
         (user-package (find-package *user-package-name*))
         (user-name    (package-name user-package))
         (user-nick    (package-nicknames user-package))
         (*package*    *package*))

    (do-clicc)
    
    ;; (unwind-protect
    ;;      (progn
    ;;        (setq *bq-level* 0)
    ;;        #+allegro-v4.1 (setq excl:*enable-package-locked-errors* nil)
    ;;        (rename-package lisp-package "original-lisp")
    ;;        #+allegro-v4.1 (setq excl:*enable-package-locked-errors* t)
    ;;        (rename-package user-package "original-user")
    ;;        (rename-package "CLICC" "original-clicc")
    ;;        (when (find-package "CLICC-CLICC")
    ;;          (rename-package "CLICC-CLICC" "CLICC"))
    ;;        (rename-package "CLICC-LISP" "COMMON-LISP")
    ;;        (rename-package "CLICC-USER" "COMMON-LISP-USER")
    ;;        (do-clicc))
    ;;   (rename-package "COMMON-LISP" "CLICC-LISP" '("L"))
    ;;   (rename-package "COMMON-LISP-USER" "CLICC-USER")
    ;;   (when (find-package "CLICC")
    ;;     #+allegro-v4.1 (rename-package "CLICC" "CLICC")
    ;;     (rename-package "CLICC" "CLICC-CLICC"))
    ;;   (rename-package "original-clicc" "CLICC")
    ;;   #+allegro-v4.1 (setq excl:*enable-package-locked-errors* nil)
    ;;   (rename-package lisp-package lisp-name lisp-nick)
    ;;   #+allegro-v4.1 (setq excl:*enable-package-locked-errors* t)
    ;;   (rename-package user-package user-name user-nick))
    ))

;;------------------------------------------------------------------------------
;; Fragt, welche in LISP geschriebenen CLICC-Laufzeitsystem-Funktionen mittels
;; clicc nach C uebersetzt werden sollen.
;;------------------------------------------------------------------------------
(defun ask-runtime ()
  (let ((*SHOW-VERSION* nil)
        (*CLICC-PRINT* t)
        (*SPLIT-FILES* *SPLIT-FILES*)
        (*ONLY-PASS1* *ONLY-PASS1*))

    (loop
     (format
      t
      "~&A)ll L)isp-module I)nline-module Q)uit~%~
      P)rint messages is ~a  S)plit is ~a  O)nly Pass1 is ~a  ~
       K)ill C-Files ~%~
       choose: "
      (if *CLICC-PRINT* "on" "off")
      (if *SPLIT-FILES* "on" "off")
      (if *ONLY-PASS1* "on" "off")) 

     (case (let ((*package* (find-package *clicc-package-name*))) (read))
           (Q (return))    
           (A (do-inline-module) (do-lisp-module) (return))
           (L (do-lisp-module))
           (I (do-inline-module))
           (O (setq *ONLY-PASS1* (not *ONLY-PASS1*)))
           (P (setq *CLICC-PRINT* (not *CLICC-PRINT*)))
           (S (setq *SPLIT-FILES* (not *SPLIT-FILES*)))
           (K (mapc #'delete-file
                    (directory (make-pathname
                                :directory *CLICC-LISP2C-DIR*
                                :name :wild
                                :type "c"))))))))

;;------------------------------------------------------------------------------
;; Pr"uft, ob Probleme durch Splitting auftreten werden
;;------------------------------------------------------------------------------
(defun check-split (unsplitted splitted)
  (not
   (cond
     (*SPLIT-FILES*
      (when (probe-file (make-pathname :directory *CLICC-LISP2C-DIR*
                                       :name unsplitted :type "c"))
        (format t "There is an unsplitted version, use K)ill first~%")
        t))
     (t (when (probe-file (make-pathname :directory *CLICC-LISP2C-DIR*
                                         :name splitted :type "c"))
          (format t "There is an splitted version, use K)ill first~%")
          t)))))

;;------------------------------------------------------------------------------
;; Compiliert das Lisp Modul des Laufzeitsystems
;;------------------------------------------------------------------------------
(defun do-lisp-module ()
  (when (check-split "lisp" "Fread")
    (let ((*C-max-line-count* nil)
          (*delete-verbosity* 2)
          (*default-pathname-defaults*
           (make-pathname :directory *CLICC-LISP-DIR*)))
      (clicc "lisp" 
             :out (make-pathname :directory *CLICC-LISP2C-DIR* :name "lisp")
             :module-compiler t
             :lisp-module t))))

;;------------------------------------------------------------------------------
;; Compiliert die Inline Funktionen, ueber die der Compiler spezielles Wissen
;; hat
;;------------------------------------------------------------------------------
(defun do-inline-module ()
  (when (check-split "inline" "Fconsp")
    (let ((*delete-verbosity* 2)
          (*default-pathname-defaults*
           (make-pathname :directory *CLICC-LISP-DIR*)))
      (clicc "inline" 
             :out (make-pathname :directory *CLICC-LISP2C-DIR* :name "inline")
             :module-compiler t
             :inline-module t
             :lisp-module t))))

;;------------------------------------------------------------------------------
(defun os-file-newer-as (file1 file2)
  (let ((file2-write-date (and (probe-file file2) (file-write-date file2))))
    (or (null file2-write-date)
        (> (file-write-date file1) file2-write-date))))


;;------------------------------------------------------------------------------
;; Der Aufruf des Compilers.
;; Resultat: T, wenn Fehler auftraten, NIL sonst.
;;------------------------------------------------------------------------------
(defun do-clicc ()
  (with-standard-io-syntax
    
    ;; Damit die Zahlen klein bleiben.
    ;;--------------------------------
    (gensym 0)                           ; f�r CltL1
    (setq *gensym-counter* 1)            ; f�r CLtL2

    (setq *FILENAME*     (merge-pathnames *filename*))
    (setq *OUT-FILENAME* (if *OUT-FILENAME*
                             (merge-pathnames *OUT-FILENAME*)
                             *FILENAME*))

    (when *SHOW-VERSION*
      (format t "~&~
;;; CLiCC, the Common Lisp to C Compiler --- Version ~a~%~
;;; Copyright (C) 1994 Christian-Albrechts-Universitaet zu Kiel~%
;;; Copyright (C) ~a Pascal J. Bourguignon~%~%"
              *CLICC-VERSION* (CLiCC-Copyright-Year)))

    (setq *NERRORS* 0 *NWARNINGS* 0)
    (p0-init)
    
    (when (= *NERRORS* 0)
      (clicc-message "Syntactic and semantic analysis")
      (pass_1))

    (unless *ONLY-PASS1*

      ;; Das Symbol T hat zunaechst den Typ SYMBOL. Wenn es von irgendwelchen
      ;; Optimierungen ins Programm eingebaut wird, sollte es den genaueren Typ
      ;; T-SYMBOL haben, um gleich weitere Optimierungen zu ermoeglichen.
      ;;-----------------------------------------------------------------
      (let ((t-sym (get-symbol-bind 'com.informatimago.clicc.lisp::T)))
        (when t-sym (setf (?type t-sym) T-SYMBOL-T)))

      ;; Die Funktion com.informatimago.clicc.lisp::error wird gesondert behandelt; es wird angenommen, da"s
      ;; sie keine Seiteneffekte verursacht.
      ;;------------------------------------
      (setq *error-function* (get-global-fun 'com.informatimago.clicc.lisp::error))

      (if *OPTIMIZE*
          (progn
            (when (= *NERRORS* 0)
              (search-and-delete-unused-objects))

            (when (and (= *NERRORS* 0)
                       (= *ITERATIONS* 1)
                       (not *no-inlining*))
              (simplify-params #'used-dynamic-vars-simple))
            
            ;; Aufruf einer sehr einfachen Typinferenz
            (when (= *NERRORS* 0)
              (let ((*ti-level* 0)
                    (*ti-verbosity* (min *ti-verbosity* 1)))
                (do-ti)))
            
            (when (= *NERRORS* 0)
              (pass_3))                 ; level-, mv-Slots etc. setzen.

            (when (= *NERRORS* 0)
              (unless *no-tail-recursion*
                (tail-rec-module)))

            (when (= *NERRORS* 0)
              (let ((*no-seo* t)

                    ;; Wenn so fr"uh bereits wesentliches optimiert werden
                    ;; kann, dann ist es verd"achtig
                    ;;------------------------------
                    (*optimize-verbosity* (max *optimize-verbosity* 2)))
                (do-optimization)))

            (when (= *NERRORS* 0)
              (search-and-delete-unused-objects))

            ;; Nach dem Inlining ist man an gel"oschten Funktionen nicht mehr so
            ;; sehr interessiert.
            (let ((*delete-verbosity* (1- *delete-verbosity*)))

              (dotimes (iteration *ITERATIONS*)
                (when (> *ITERATIONS* 1)
                  (clicc-message-line 28)
                  (clicc-message "~D. iteration step" (1+ iteration))
                  (clicc-message-line 28)
                  (clicc-message " "))
                
                (when (= *NERRORS* 0)
                  (do-inline))
                
                (when (= *NERRORS* 0)
                  (search-and-delete-unused-objects))
                
                (when (= *NERRORS* 0)
                  (search-fun-calls))
                
                (when (= *NERRORS* 0)
                  (pass_3))
                
                ;; Aufruf der Seiteneffektanalyse
                (unless *NO-SIDE-EFFECT-ANALYSIS*
                  (when (= *NERRORS* 0)
                    (analyse-module)))
                
                ;; Aufruf der Typinferenz
                (when (= *NERRORS* 0)
                  (reset-type-annotations)
                  (do-ti))
                
                (when (= *NERRORS* 0)
                  (do-optimization))
                
                (when (= *NERRORS* 0)
                  (search-and-delete-unused-objects))

                (when (and (= *NERRORS* 0)
                           (> *ITERATIONS* 1)
                           (= iteration 0)
                           (not *no-inlining*))
                  (simplify-params #'used-dynamic-vars-with-side-effects)
                  (pass_3))))
            
            ;; Suche nach Typfehlern
            (when (= *NERRORS* 0)
              (look-for-type-errors *module*))

            (when (and *module-compiler*
                       (not *inline-module*)
                       (not *no-inlining*)
                       (= 0 *NERRORS*))
              (clicc-message "Preparing Module Interface Specification")
              (prepare-export-write)))

          ;; Wenn nicht optimiert wird, muessen zumindest die named-consts,
          ;; die als Referenzen auf Funktionen gedient haben, ersetzt werden.
          ;; Ausserdem muessen noch die angewandten Vorkommen gezaehlt werden.
          (let ((*no-to* t)
                (*no-seo* t)
                (*no-simp* t))
            (set-used-slots-for-cg)
            (pass_3)
            (do-optimization)))
      
      (when (= *NERRORS* 0)
        (clicc-message "Annotation for code generation")
        (pass_3))

      (unless *NO-CODEGEN*
        (when (= *NERRORS* 0)
          (clicc-message "Code Generation")
          (codegen)))

      (when (and *module-compiler* (not *inline-module*))
        (when (= *NERRORS* 0)
          (clicc-message "Writing Module Interface Specification")
          (export-write))))

    (when (or (> *NERRORS* 0) (> *NWARNINGS* 0))
      (format t ";;; ~D Errors, ~D Warnings~%" *NERRORS*  *NWARNINGS*))
    (terpri)

    (> *NERRORS* 0)))

;;------------------------------------------------------------------------------
;;;; THE END ;;;;
