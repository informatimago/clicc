(ql:quickload :com.informatimago.clicc.compiler)
(in-package :com.informatimago.clicc)
(with-input-from-string  (*standard-input* (format nil "A~%"))
  (ask-runtime))
;; Fails with:
;;    Error: A call to CONS should have at most 2 arguments.
;; Because LIST* has the same operator function as CONS.
