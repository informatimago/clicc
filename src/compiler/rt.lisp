'(
  #+LUCID
  (progn
    (import '(lcl:quit lcl:cd lcl:pwd))
    (setq *compiled-extension* (car lcl:*load-binary-pathname-types*)))
  #+EXCL
  (progn
    (import '(excl:exit excl:chdir excl:current-directory))
    (setq *compiled-extension* excl:*fasl-default-type*))
  #+KCL
  (progn 
    (setq *lisp-extension* "lsp")
    (setq *compiled-extension* "o"))
  #+CMU
  (progn
    (import '(extensions:quit))
    (setq *compiled-extension* (c:backend-fasl-file-type c:*backend*)))
  #+CLISP
  (progn
    ;; type must be upper case under DOS
    (setq *lisp-extension* (pathname-type "any.lsp"))
    (setq *compiled-extension* (pathname-type "any.fas")))
  #+LISPWORKS
  (setq *compiled-extension system::*binary-file-type*)
  )
