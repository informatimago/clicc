(eval-when (:compile-toplevel :load-toplevel)
  (error "~A should be loaded, not compiled." (or *load-pathname* "This file")))

(defparameter *this-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (truename (or *load-pathname* #P"./"))))

(ccl:setenv "CLICCROOT" (namestring
                         (merge-pathnames
                          (make-pathname :directory '(:relative :up :up)
                                         :name nil :type nil :version nil
                                         :defaults *this-directory*)
                          *this-directory* nil)))

(pushnew *this-directory* asdf:*central-registry*)
