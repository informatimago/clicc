;;;-----------------------------------------------------------------------------
;;; CLiCC: The Common Lisp to C Compiler
;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;-----------------------------------------------------------------------------
;;; CLiCC has been developed as part of the APPLY research project,
;;; funded by the German Ministry of Research and Technology.
;;; 
;;; CLiCC is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; CLiCC is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License in file COPYING for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;-----------------------------------------------------------------------------
;;; Funktion : Typdeklarationen der Funktionstypen importierter Funktionen.
;;;
;;; $Revision: 1.50 $
;;; $Id: tiimpdec.lisp,v 1.50 1994/12/17 11:55:26 pm Exp $
;;;-----------------------------------------------------------------------------

(in-package "COM.INFORMATIMAGO.CLICC")

;;------------------------------------------------------------------------------
;; Beim jedem Laden dieser Datei wird dieser Schalter zur�ckgesetzt, weil sich
;; Funktionsbeschreibungen ge�ndert haben k�nnten.
;;------------------------------------------------------------------------------
(setf *ti-type-declarations-are-initialized* nil) 


;;------------------------------------------------------------------------------
;; Einige h�ufig verwendete Typabstraktionsfunktionen:
;;------------------------------------------------------------------------------
(defun the-first-type (type1 &rest other-types)
  (declare (ignore other-types))
  type1)

;;------------------------------------------------------------------------------
(defun the-second-type (type1 type2 &rest other-types)
  (declare (ignore type1 other-types))
  type2)


;;------------------------------------------------------------------------------
(defun car-type (type)
  (zs-typecase type
               (bottom-t                  bottom-t)
               ((conform non-list-cons-t) top-t)
               ((conform null-t)          (type-join null-t 
                                                     (list-component type)))
               (otherwise                 (list-component type))))


;;------------------------------------------------------------------------------
(defun %car-type (type)
  (zs-typecase type
               (bottom-t                  bottom-t)
               ((conform non-list-cons-t) top-t)
               (otherwise                 (list-component type))))


;;------------------------------------------------------------------------------
(defun cdr-type (type)
  (zs-typecase type
               (bottom-t                  bottom-t)
               (list-t                    (type-join type null-t))
               (non-list-cons-t           not-list-t)
               ((conform non-list-cons-t) top-t)
               (otherwise                 (list-of (list-component type)))))


;;------------------------------------------------------------------------------
(defun caar-type (type)
  (car-type (car-type type)))

(defun cadr-type (type)
  (car-type (cdr-type type)))

(defun cdar-type (type)
  (cdr-type (car-type type)))

(defun cddr-type (type)
  (cdr-type (cdr-type type)))

(defun caaar-type (type)
  (car-type (car-type (car-type type))))

(defun caadr-type (type)
  (car-type (car-type (cdr-type type))))

(defun cadar-type (type)
  (car-type (cdr-type (car-type type))))

(defun caddr-type (type)
  (car-type (cdr-type (cdr-type type))))

(defun cdaar-type (type)
  (cdr-type (car-type (car-type type))))

(defun cdadr-type (type)
  (cdr-type (car-type (cdr-type type))))

(defun cddar-type (type)
  (cdr-type (cdr-type (car-type type))))

(defun cdddr-type (type)
  (cdr-type (cdr-type (cdr-type type))))

(defun cadddr-type (type)
  (car-type (cdr-type (cdr-type (cdr-type type)))))


;;------------------------------------------------------------------------------
(defun cons-type (car-type cdr-type)
  (if (zs-subtypep cdr-type list-t)
      (type-meet list-cons-t
                 (type-join (list-cons-of car-type) 
                            cdr-type))
      cons-t))


;;------------------------------------------------------------------------------
(defun replace-type (cons-type element-type)
  (declare (ignore element-type))
  (type-meet cons-t cons-type))


;;------------------------------------------------------------------------------
(defun member-type (arg1-type list-type &rest other-types)
  (declare (ignore arg1-type other-types))
  (type-meet all-list-t
             (type-join null-t
                        list-type)))

;;------------------------------------------------------------------------------
(defun setf-type  (type1 &rest other-types)
  (declare (ignore other-types))
  type1)


;;------------------------------------------------------------------------------
(defun add-multiplication-op (&rest types)
  (cond ((endp types) 
         fixnum-t)
        ((endp (rest types))
         (type-meet (first types) number-t))
        (T
         (zs-typecase (apply #'multiple-type-join types)
                      (bottom-t           bottom-t)
                      (byte-t             word-t)
                      (word-t             fixnum-t)
                      (integer-t          integer-t)
                      (float-t            float-t)
                      ((conform number-t) number-t)
                      (otherwise          bottom-t)))))


;;------------------------------------------------------------------------------
(defun inc-dec-op (type)
  (zs-typecase type
               (bottom-t           bottom-t)
               (byte-t             word-t)
               (word-t             fixnum-t)
               (integer-t          integer-t)
               (float-t            float-t)
               ((conform number-t) number-t)
               (otherwise          bottom-t)))


;;------------------------------------------------------------------------------
(defun division-op (&rest types)
  (zs-typecase (apply #'multiple-type-join types)
               (bottom-t            bottom-t)
               (float-t             float-t)
               ((conform number-t)  number-t)
               (otherwise           bottom-t)))


;;------------------------------------------------------------------------------
(defun round-fun (number &optional divisor)
  (declare (ignore divisor))
  (type-meet integer-t number))


;;------------------------------------------------------------------------------
(defun numbers-to-number (&rest argument-types)
  (let ((join (apply #'multiple-type-join argument-types)))
    (if (types-are-conform join number-t)
        (type-meet number-t join)
        bottom-t)))

;;------------------------------------------------------------------------------
(defun assert-sequence-t (type)
  (type-meet sequence-t type))


;;------------------------------------------------------------------------------
(defun remove-fun (item-type sequence-type &rest types)
  (declare (ignore item-type types))
  (zs-typecase sequence-type
               (bottom-t             bottom-t)
               (list-t               (type-join null-t sequence-type))
               ((conform sequence-t) (type-join null-t (type-meet sequence-t   
                                                                  sequence-type)))
               (otherwise            bottom-t)))


;;------------------------------------------------------------------------------
(defun find-fun (item-type sequence-type &rest types)
  ;; Falls das Element nicht gefunden wird, wird nil geliefert.
  (type-join null-t 
             (if (null types)
                 ;; Wenn keine Schluesselworte angegeben werden, dann ist
                 ;; das Ergebnis das gesuchte item.
                 item-type
                 
                 ;; Ansonsten wird ein Element der sequence geliefert.
                 (zs-typecase sequence-type
                              (bottom-t             bottom-t)
                              (list-t               (list-component sequence-type))
                              ((conform sequence-t) top-t)
                              (otherwise            bottom-t)))))


;;------------------------------------------------------------------------------
(defun joined-list-type (type1 type2 &rest types)
  (declare (ignore types))
  (type-meet list-t (type-join type1 type2)))


;;------------------------------------------------------------------------------
(defun abort-function (&rest types)
  (declare (ignore types))
  (set-all-type-bindings-to bottom-t)
  bottom-t)


;;------------------------------------------------------------------------------
;; Initialisiere die Typbeschreibungen der importierten Funktionen. Diese
;; Funktion wird ueberfluessig, wenn die Typbeschreibungen in sys-fun.def
;; oder in den importierten Funktionen zugeordneten Definitionsdateien 
;; festgehalten werden.
;;------------------------------------------------------------------------------
(defun initialize-function-descriptions-part1 ()
  ;; 6.2.2. Specific Data Type Predicates
  ;; ------------------------------------
  (declare-type-predicates ((COM.INFORMATIMAGO.CLICC.LISP::not                null-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::endp               null-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::symbolp            symbol-t)
                            (COM.INFORMATIMAGO.CLICC.RT::symp              non-null-sym-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::atom               atom-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::consp              cons-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::listp              all-list-t) 

                            (COM.INFORMATIMAGO.CLICC.RT::fixnump           fixnum-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::integerp           integer-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::floatp             float-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::numberp            number-t)

                            (COM.INFORMATIMAGO.CLICC.LISP::characterp         character-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::stringp            string-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::vectorp            vector-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::simple-vector-p    vector-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::simple-string-p    string-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::arrayp             array-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::simple-array-p     array-t)
                            (COM.INFORMATIMAGO.CLICC.RT::plain-vector-p    array-t)

                            (COM.INFORMATIMAGO.CLICC.LISP::functionp          function-t)
                            (COM.INFORMATIMAGO.CLICC.RT::structp           structure-t)
                            (COM.INFORMATIMAGO.CLICC.RT::instancep         class-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::packagep           package-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::streamp            stream-t)
                            (COM.INFORMATIMAGO.CLICC.LISP::hash-table-p       hash-table-t)))
  )


;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
(defun initialize-function-descriptions-part2 ()

  ;; 6.3. Equality Predicates
  ;; ------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::eq     (top-t top-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::eql    (top-t top-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::equal  (top-t top-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::equalp (top-t top-t) -> bool-t)
  

  ;; 7.1.1. Reference
  ;; ----------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::symbol-value     (symbol-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::symbol-value    (symbol-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::boundp           (symbol-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::unbound-value-p (top-t)    -> bool-t)

  
  ;; 7.1.2. Assignment
  ;; -----------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::set                  (symbol-t top-t) -> top-t #'the-second-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.RT::symbol-value) (top-t symbol-t) -> top-t #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::makunbound           (symbol-t)       -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::makunbound          (symbol-t)       -> symbol-t)
  
  ;;  7.9. Multiple Values
  ;; ---------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::values        () -> top-t   #'(lambda (&rest types) 
                                                                          (if types (first types) null-t)))

  ;; 10.1. The Property List
  ;; -----------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::get          (symbol-t top-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::remprop      (symbol-t top-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::symbol-plist (symbol-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::symbol-plist (symbol-t)      -> list-t)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.RT::symbol-plist) (list-t symbol-t) -> list-t 
            #'the-first-type)
  
  ;; 10.2. The Print Adr
  ;; --------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::symbol-name  (symbol-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::symbol-name (symbol-t) -> string-t)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.RT::symbol-name) (string-t symbol-t) -> string-t)
  
  ;; 10.3. Creating Symbols
  ;; ----------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-symbol    (string-t) -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-symbol   ()         -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::copy-symbol    (symbol-t) -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::gensym         ()         -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::gentemp        ()         -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::symbol-package (symbol-t) -> (type-join package-t null-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::keywordp       (top-t)    -> bool-t)

  (dec-type COM.INFORMATIMAGO.CLICC.RT::symbol-package-index (symbol-t) -> (type-join null-t integer-t))
  (dec-type COM.INFORMATIMAGO.CLICC.RT::constant-flag-p (symbol-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::set-constant-flag (symbol-t) -> symbol-t)
  
  ;; 11.7. Package System Functions and Variables
  ;; --------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.RT::ensure-package      (package-name-t)    -> package-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-package         (package-name-t)    -> package-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::in-package           (package-name-t)    -> package-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find-package         (package-or-name-t) -> 
            (type-join null-t package-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::package-name         (package-or-name-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::package-nicknames    (package-or-name-t) -> (list-of string-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::rename-package       (package-or-name-t package-name-t) 
            -> package-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::package-use-list     (package-or-name-t)  -> (list-of package-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::package-used-by-list (package-or-name-t)  -> (list-of package-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::list-all-packages    ()                   -> (list-of package-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::package-shadowing-symbols (package-or-name-t) 
            -> (list-of symbol-t))

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::intern           (string-t package-or-name-t) -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find-symbol      (string-t package-or-name-t) -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::unintern         (symbol-t package-or-name-t) -> bool-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::export           ((list-of symbol-t) package-or-name-t) 
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::unexport         ((list-of symbol-t) package-or-name-t) 
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::import           ((list-of symbol-t) package-or-name-t) 
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::shadowing-import ((list-of symbol-t) package-or-name-t) 
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::shadow           ((list-of symbol-t) package-or-name-t) 
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::use-package      ((type-join (list-of package-or-name-t) 
                                                                       package-or-name-t)
                                                            package-or-name-t)
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::unuse-package    ((type-join (list-of package-or-name-t) 
                                                                       package-or-name-t)
                                                            package-or-name-t)
            -> t-symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find-all-symbols () -> (list-of symbol-t))


  ;; 12.2. Predicates on Numbers
  ;; ---------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::zerop  (number-t)  -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::plusp  (number-t)  -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::minusp (number-t)  -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::oddp   (integer-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::evenp  (integer-t) -> bool-t)
  
  
  ;; 12.3. Comparisons on Numbers
  ;; ----------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::=    (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::/=   (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::<    (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::<fix (fixnum-t fixnum-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::>    (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::<=   (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::>=   (number-t number-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::min  (number-t number-t) -> number-t  #'numbers-to-number)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::max  (number-t number-t) -> number-t  #'numbers-to-number)

  ;; 12.4. Arithmetic Operations
  ;; ---------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::+      (number-t number-t) -> number-t  #'add-multiplication-op)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::-      (number-t number-t) -> number-t  #'add-multiplication-op)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::*      (number-t number-t) -> number-t  #'add-multiplication-op)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::1+     (number-t)          -> number-t  #'inc-dec-op)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::1-     (number-t)          -> number-t  #'inc-dec-op)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::1-fix (fixnum-t)          -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::/      (number-t number-t) -> number-t  #'division-op)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::abs    (number-t)          -> number-t)

  (dec-type COM.INFORMATIMAGO.CLICC.RT::log               (number-t number-t)   -> number-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::float             (number-t)            -> float-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::floor             (number-t number-t)   -> number-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::ceiling           (number-t number-t)   -> number-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::truncate          (number-t number-t)   -> number-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::round             (number-t number-t)   -> number-t)
  
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%logior           (fixnum-t fixnum-t)   -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%logxor           (fixnum-t fixnum-t)   -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%logand           (fixnum-t fixnum-t)   -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%lognot           (fixnum-t)            -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%shift-right      (integer-t fixnum-t)  -> integer-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::ash                (integer-t fixnum-t)  -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logxor             (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logior             (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logand             (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logandc1           (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logandc2           (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::lognand            (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logorc1            (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::logorc2            (integer-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::lognot             (integer-t)           -> integer-t)


  ;; 12.6. Type Conversions and Component Extractions on Numbers
  ;; -----------------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::float    (number-t float-t)  -> float-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::floor    (number-t number-t) -> number-t  #'round-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::ceiling  (number-t number-t) -> number-t  #'round-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::truncate (number-t number-t) -> number-t  #'round-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::round    (number-t number-t) -> number-t  #'round-fun)


  ;; 13.2. Predicates on Characters
  ;; ------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::standard-char-p    (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::graphic-char-p     (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::alpha-char-p       (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::upper-case-p       (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::lower-case-p       (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::both-case-p        (character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::digit-char-p       (character-t) -> (type-join null-t fixnum-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::alphanumericp      (character-t) -> bool-t)
  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char=              (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char/=             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char<              (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char>              (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char<=             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char>=             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-equal         (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-not-equal     (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-lessp         (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-greaterp      (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-not-greaterp  (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-not-lessp     (character-t character-t) -> bool-t)

  (dec-type COM.INFORMATIMAGO.CLICC.RT::char=             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char/=            (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char<             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char>             (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char<=            (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char>=            (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-equal        (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-not-equal    (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-lessp        (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-greaterp     (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-not-greaterp (character-t character-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-not-lessp    (character-t character-t) -> bool-t)

  ;; 13.3. Character Construction and Selection
  ;; ------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-code          (character-t) -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::code-char          (fixnum-t)    -> or-null-character-t)
                                        ; (dec-type COM.INFORMATIMAGO.CLICC.LISP::character          (top-t)       -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-upcase        (character-t) -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-downcase      (character-t) -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::digit-char         (integer-t)   -> or-null-character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char-name          (character-t) -> (type-join null-t string-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::name-char          (top-t) -> top-t
            #'(lambda (type) (type-join null-t type)))

  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-code         (character-t) -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::code-char         (fixnum-t)    -> or-null-character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-upcase       (character-t) -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::char-downcase     (character-t) -> character-t)

  
  ;; 14. Sequences
  ;; -------------



  ;; 14.1. Simple Sequence Functions
  ;; -------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::elt        (sequence-t integer-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::subseq     (sequence-t integer-t integer-t) -> top-t
            #'(lambda (sequence-type &rest other-types)
                (declare (ignore other-types))
                (if (zs-subtypep sequence-type all-list-t) 
                    (type-meet sequence-type list-t)
                    (type-meet sequence-type sequence-t))))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::copy-seq   (sequence-t) -> sequence-t  #'assert-sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::reverse    (sequence-t) -> sequence-t  #'assert-sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nreverse   (sequence-t) -> sequence-t  #'assert-sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::length     (sequence-t) -> integer-t)


  ;; 14.2. Concatenating, Mapping, and Reducing Sequences
  ;; ----------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::concatenate (top-t sequence-t sequence-t) -> sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::map         (top-t function-t sequence-t) -> sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::some        (function-t sequence-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::every       (function-t sequence-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::notany      (function-t sequence-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::notevery    (function-t sequence-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::reduce      (function-t sequence-t) -> top-t)

  ;; Die Funktionen mapcar, maplist, mapc und mapl sind speziell getypt. 
  ;; Die zugeh�rigen Typabstraktionsfunktionen liegen tipass2.lisp.



  ;; 14.3. Modifying Sequences
  ;; -------------------------
  ;;(fill        )
  ;;(replace     )

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::remove        (top-t      sequence-t) -> sequence-t #'remove-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::remove-if     (function-t sequence-t) -> sequence-t #'remove-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::remove-if-not (function-t sequence-t) -> sequence-t #'remove-fun)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::delete        (top-t      sequence-t) -> sequence-t #'remove-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::delete-if     (function-t sequence-t) -> sequence-t #'remove-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::delete-if-not (function-t sequence-t) -> sequence-t #'remove-fun)


  (dec-type COM.INFORMATIMAGO.CLICC.LISP::remove-duplicates (sequence-t) -> sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::delete-duplicates (sequence-t) -> sequence-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::substitute        (top-t top-t sequence-t) -> sequence-t
            #'(lambda (type1 type2 sequence &rest types)
                (declare (ignore type1 type2 types))
                (type-meet sequence sequence-t)))
  ;; .....

  ;; 14.4. Searching Sequences for Items
  ;; -----------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find        (top-t      sequence-t) -> top-t  #'find-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find-if     (function-t sequence-t) -> top-t  #'find-fun)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::find-if-not (function-t sequence-t) -> top-t  #'find-fun)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::position        (top-t      sequence-t) -> 
            (type-join null-t integer-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::position-if     (function-t sequence-t) -> 
            (type-join null-t integer-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::position-if-not (function-t sequence-t) -> 
            (type-join null-t integer-t))
  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::count           (top-t      sequence-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::count-if        (function-t sequence-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::count-if-not    (function-t sequence-t) -> integer-t)


  ;; 14.5. Sorting and Merging
  ;; -------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::sort            (sequence-t function-t) -> sequence-t
            #'(lambda (sequence-type &rest other-types)
                (declare (ignore other-types))
                (type-meet sequence-t sequence-type)))



  ) ; initialize-function-descriptions-part2


;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
(defun initialize-function-descriptions-part3 ()
  ;;----------------------------------------------------------------------------
  
  ;; 15.1. Conses
  ;; ------------
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%car    (cons-t)       -> top-t   #'%car-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%cdr    (cons-t)       -> top-t   #'cdr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%rplaca (cons-t top-t) -> cons-t  #'replace-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::%rplacd (cons-t top-t) -> cons-t  #'replace-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::rplaca   (cons-t top-t) -> cons-t  #'replace-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::rplacd   (cons-t top-t) -> cons-t  #'replace-type)
  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::car      (all-list-t) -> top-t   #'car-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cdr      (all-list-t) -> top-t   #'cdr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::caar     (all-list-t) -> top-t   #'caar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cadr     (all-list-t) -> top-t   #'cadr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cdar     (all-list-t) -> top-t   #'cdar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cddr     (all-list-t) -> top-t   #'cddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::caaar    (all-list-t) -> top-t   #'caaar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::caadr    (all-list-t) -> top-t   #'caadr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cadar    (all-list-t) -> top-t   #'cadar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::caddr    (all-list-t) -> top-t   #'caddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cdaar    (all-list-t) -> top-t   #'cdaar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cdadr    (all-list-t) -> top-t   #'cdadr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cddar    (all-list-t) -> top-t   #'cddar-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cdddr    (all-list-t) -> top-t   #'cdddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cadddr   (all-list-t) -> top-t   #'cadddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::first    (all-list-t) -> top-t   #'car-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::second   (all-list-t) -> top-t   #'cadr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::third    (all-list-t) -> top-t   #'caddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::fourth   (all-list-t) -> top-t   #'cadddr-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::rest     (all-list-t) -> top-t   #'cdr-type)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::cons        (top-t top-t)   -> cons-t #'cons-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::tree-equal  (list-t list-t) -> bool-t)

  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::car)    (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::cdr)    (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::caar)   (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::cadr)   (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::cdar)   (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::cddr)   (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::caddr)  (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::first)  (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::second) (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::third)  (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::fourth) (top-t cons-t) -> cons-t  #'setf-type)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::rest)   (top-t cons-t) -> cons-t  #'setf-type)


  ;; 15.2. Lists
  ;; -----------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::list-length (list-t)               -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nth         (integer-t all-list-t) -> top-t
            #'(lambda (integer-type cons-type)
                (declare (ignore integer-type))
                (car-type cons-type)))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nthcdr      (integer-t all-list-t) -> all-list-t
            #'(lambda (integer-type cons-type)
                (declare (ignore integer-type))
                (cdr-type cons-type)))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::last      (all-list-t integer-t) -> all-list-t  #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::list      () -> list-t  #'list-cons-of)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::list*     () -> list-t  #'list-cons-of)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::append    (all-list-t all-list-t) -> all-list-t
            #'(lambda (&rest types)
                (type-meet list-t 
                           (apply #'multiple-type-join types))))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::copy-list   (all-list-t) -> all-list-t  #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::copy-alist  (all-list-t) -> all-list-t  #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::assoc       (top-t all-list-t) -> all-list-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::butlast     (all-list-t integer-t) -> all-list-t
            #'(lambda (list-type &optional integer-type) 
                (declare (ignore integer-type))
                (type-meet list-t (type-join null-t 
                                             list-type))))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::ldiff       (all-list-t all-list-t) -> all-list-t
            #'(lambda (type1 type2)
                (type-meet list-t (type-meet type1 type2))))

  ;; 15.5. Using Lists as Sets
  ;; -------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::member         (top-t      all-list-t) -> top-t
            #'member-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::member-if      (function-t all-list-t) -> top-t
            #'member-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::member-if-not  (function-t all-list-t) -> top-t
            #'member-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::tailp          (all-list-t all-list-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::adjoin         (top-t all-list-t) -> cons-t
            #'(lambda (new list &rest other-types)
                (declare (ignore other-types))
                (type-meet list-cons-t
                           (type-join list 
                                      (list-cons-of new)))))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::union          (all-list-t all-list-t) -> list-t
            #'joined-list-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::intersection   (all-list-t all-list-t) -> list-t
            #'(lambda (t1 t2 &rest other-types)
                (declare (ignore other-types))
                (type-meet list-t (type-meet t1 t2))))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::set-difference (all-list-t all-list-t) -> list-t
            #'(lambda (t1 t2 &rest other-types)
                (declare (ignore t2 other-types))
                (type-meet list-t t1)))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::subsetp        (all-list-t all-list-t) -> bool-t)

  ) ; initialize-function-descriptions-part3



;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
(defun initialize-function-descriptions-part4 ()

  ;; 18.2. String Comparison
  ;; -----------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string=             (or-sym-str-char-t or-sym-str-char-t) 
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-equal        (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string<             (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string>             (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string<=            (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string>=            (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string/=            (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-lessp        (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-greaterp     (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-not-greaterp (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-not-lessp    (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-not-equal    (or-sym-str-char-t or-sym-str-char-t)
            -> bool-t)

  ;; 18.3. String Construction and Manipulation
  ;; ------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-string        (integer-t)                    -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-trim        (sequence-t or-sym-str-char-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-left-trim   (sequence-t or-sym-str-char-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-right-trim  (sequence-t or-sym-str-char-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-upcase      (or-sym-str-char-t)            -> string-t) 
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-downcase    (or-sym-str-char-t)            -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-capitalize  (or-sym-str-char-t)            -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nstring-upcase     (or-symbol-string-t)           -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nstring-downcase   (or-symbol-string-t)           -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::nstring-capitalize (or-symbol-string-t)           -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string             (or-sym-str-char-t)            -> string-t)

  ;; 22.3.1 Output to Character Streams
  ;;-----------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::write  (top-t)             -> top-t    #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::prin1  (top-t my-stream-t) -> top-t    #'the-first-type)  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::print  (top-t my-stream-t) -> top-t    #'the-first-type)  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::pprint (top-t my-stream-t) -> top-t    #'the-first-type)  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::princ  (top-t my-stream-t) -> top-t    #'the-first-type)  
  
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::write-to-string (top-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::prin1-to-string (top-t) -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::princ-to-string (top-t) -> string-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::write-char   (character-t my-stream-t) -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::write-string (string-t my-stream-t)    -> string-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::terpri       (my-stream-t)             -> null-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::fresh-line   (my-stream-t)             -> bool-t)

  ;;----------------------------------------------------------------------------
  ;; Other
  ;;----------------------------------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::read       (my-stream-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::read-line  (my-stream-t) -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::read-char  (my-stream-t) -> top-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::format     (my-stream-t string-t) -> or-symbol-string-t 
            #'(lambda (stream-type &rest types)
                (declare (ignore types)) 
                (zs-typecase stream-type
                             (bottom-t   bottom-t)
                             (null-t     string-t)
                             (t-symbol-t null-t)
                             (stream-t   null-t)
                             ((conform my-stream-t) (type-join string-t null-t))
                             (otherwise  bottom-t))))

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::error           (string-t) -> bottom-t #'abort-function)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::the-type-error ()         -> bottom-t #'abort-function)


  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-stream    () -> stream-t)


  
  ;;----------------------------------------------------------------------------
  ;; Strukturen
  ;;----------------------------------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.RT::structp          (top-t)             -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::struct-typep     (top-t    symbol-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::new-struct       (fixnum-t)          -> structure-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-struct      (symbol-t)          -> structure-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::struct-type      (structure-t)       -> symbol-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::struct-size      (structure-t)       -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::struct-ref       (structure-t fixnum-t symbol-t) -> top-t)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.RT::struct-ref) (top-t structure-t fixnum-t symbol-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::structure-ref    (structure-t fixnum-t) -> top-t)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.RT::structure-ref) (top-t structure-t fixnum-t) -> top-t
            #'the-first-type)

  ;;----------------------------------------------------------------------------
  ;; Arrays 
  ;;----------------------------------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-vector-t      (integer-t) -> vector-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-vector-fixnum (integer-t) -> vector-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-vector-float  (integer-t) -> vector-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-vector-char   (integer-t character-t) -> vector-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-vector-bit    (integer-t) -> vector-t)

  (dec-type COM.INFORMATIMAGO.CLICC.RT::row-major-aref-internal     (array-t integer-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::set-row-major-aref-internal (top-t array-t integer-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::svref-internal              (vector-t integer-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::set-svref-internal          (top-t vector-t integer-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::pvref                       (vector-t integer-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::set-pvref                   (top-t vector-t integer-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::plain-vector-length         (vector-t)           -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::plain-vector-element-code   (vector-t)           -> fixnum-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::check-array                 ()                   -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-array       ((type-join integer-t (list-of integer-t))) -> 
            array-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::array-dimensions (array-t) -> (list-of integer-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::array-rank       (array-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::array-total-size (array-t) -> integer-t)
  


  (dec-type COM.INFORMATIMAGO.CLICC.LISP::char             (string-t integer-t) -> character-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::schar            (string-t integer-t) -> character-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-hash-table  () -> hash-table-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::maphash          (function-t hash-table-t) -> null-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::string-hash      (string-t integer-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::sxhash           (top-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::sxhash-string   (string-t) -> integer-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::combine-hash    (integer-t integer-t) -> integer-t)

  (dec-type COM.INFORMATIMAGO.CLICC.LISP::y-or-n-p         (string-t) -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::yes-or-no-p      (string-t) -> bool-t)

  ;;----------------------------------------------------------------------------
  ;; 23.3 Renaming, deleting, and other file operations
  ;;----------------------------------------------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::delete-file      (file-t)   -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::probe-file       (file-t)   -> (type-join null-t pathname-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::open             (file-t)   -> (type-join null-t stream-t))
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::close            (stream-t) -> top-t)


  ;; 28. Common Lisp Object System (clos.c)
  ;;--------------------------------------
  (dec-type COM.INFORMATIMAGO.CLICC.RT::make-instance      (fixnum-t) -> class-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::instance-ref       (class-t fixnum-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::instance-set       (top-t class-t fixnum-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::set-slot-unbound   (fixnum-t class-t) -> fixnum-t)

  ;; (clos.lisp)
  ;;------------
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::make-instance        (class-t)                      -> class-t)
  (dec-type COM.INFORMATIMAGO.CLICC.RT::typep-class         (top-t class-t)                -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::slot-value           (class-t other-symbol-t)       -> top-t)
  (dec-type (setf COM.INFORMATIMAGO.CLICC.LISP::slot-value)    (top-t class-t other-symbol-t) -> top-t
            #'the-first-type)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::slot-boundp          (class-t other-symbol-t)       -> bool-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::slot-makunbound      (class-t other-symbol-t)       -> top-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::class-of             (class-t)                      -> class-t)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::no-next-method       () -> bottom-t  #'abort-function)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::no-applicable-method () -> bottom-t  #'abort-function)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::slot-missing         (class-t class-t other-symbol-t) -> bottom-t
            #'abort-function)
  (dec-type COM.INFORMATIMAGO.CLICC.LISP::slot-unbound         (class-t class-t top-t)          -> bottom-t
            #'abort-function)


  ;;----------------------------------------------------------------------------
  ;; Funktionen zum Erzeugen von Lisp-Werten aus C-Werten
  ;;----------------------------------------------------------------------------
  ;; (dec-type <fun> (<arg-types>) -> <ret-type>)
  




  ) ; initialize-function-descriptions-part4


;;------------------------------------------------------------------------------
;;;; THE END ;;;;

