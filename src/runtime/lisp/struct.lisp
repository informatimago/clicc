;;;-----------------------------------------------------------------------------
;;; CLiCC: The Common Lisp to C Compiler
;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;-----------------------------------------------------------------------------
;;; CLiCC has been developed as part of the APPLY research project,
;;; funded by the German Ministry of Research and Technology.
;;; 
;;; CLiCC is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; CLiCC is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License in file COPYING for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;-----------------------------------------------------------------------------
;;; Function : Structures
;;;
;;; $Revision: 1.15 $
;;; $Id: struct.lisp,v 1.15 1994/11/22 14:55:56 hk Exp $
;;;----------------------------------------------------------------------------

(com.informatimago.clicc::in-package "COM.INFORMATIMAGO.CLICC.LISP")

;;------------------------------------------------------------------------------
;; F�r Fehlermeldungen
;;------------------------------------------------------------------------------
(defconstant NO_STRUCT "~S is not a structure of type ~S")

;;------------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT Datentyp
;;------------------------------------------------------------------------------
(deftype com.informatimago.clicc.rt:struct () '(satisfies com.informatimago.clicc.rt::structp))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT-TYPEP object type
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:struct-typep (object type)
  (if (com.informatimago.clicc.rt::structp object)
      (let ((struct-type (com.informatimago.clicc.rt::struct-type object)))
        (loop
         (when (eq type struct-type)
           (return object))
         (unless (setq struct-type (get struct-type 'com.informatimago.clicc.rt::included-struct))
           (return nil))))))

;;------------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT-TYPE structure
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:struct-type (structure)
  (com.informatimago.clicc.rt::structure-ref structure -1))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::MAKE-STRUCT type &rest slot-values
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:make-struct (type &rest slot-values)
  (let ((new-structure (com.informatimago.clicc.rt::new-struct (length slot-values))))
    (setf (com.informatimago.clicc.rt::structure-ref new-structure -1) type)
    (let ((i 0))
      (dolist (slot slot-values new-structure)
        (setf (com.informatimago.clicc.rt::structure-ref new-structure i) slot)
        (incf i)))))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::COPY-STRUCT structure type
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:copy-struct (structure type)
  (unless (com.informatimago.clicc.rt:struct-typep structure type)
    (error NO_STRUCT structure type))
  (let* ((structure-size (com.informatimago.clicc.rt::struct-size structure))
         (copy-of-structure (com.informatimago.clicc.rt::new-struct structure-size)))
    (setf (com.informatimago.clicc.rt::structure-ref copy-of-structure -1) type)
    (dotimes (index structure-size copy-of-structure)
      (setf (com.informatimago.clicc.rt::structure-ref copy-of-structure index)
            (com.informatimago.clicc.rt::structure-ref structure index)))))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT-REF structure index type
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:struct-ref (structure index type)
  (if (com.informatimago.clicc.rt:struct-typep structure type)
      (com.informatimago.clicc.rt::structure-ref structure index)
      (error NO_STRUCT structure type)))

;;-----------------------------------------------------------------------------
;; (SETF COM.INFORMATIMAGO.CLICC.RT::STRUCT-REF) newvalue structure index type
;;-----------------------------------------------------------------------------
(defun (setf com.informatimago.clicc.rt:struct-ref) (newvalue structure index type)
  (if (com.informatimago.clicc.rt:struct-typep structure type)
      (setf (com.informatimago.clicc.rt::structure-ref structure index) newvalue)
      (error NO_STRUCT structure type)))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT-CONSTRUCTOR symbol
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:struct-constructor (symbol)
  (get symbol 'com.informatimago.clicc.rt::struct-constructor))

;;-----------------------------------------------------------------------------
;; (SETF COM.INFORMATIMAGO.CLICC.RT::STRUCT-CONSTRUCTOR) constructor symbol
;;-----------------------------------------------------------------------------
(defun (setf com.informatimago.clicc.rt:struct-constructor) (constructor symbol)
  (setf (get symbol 'com.informatimago.clicc.rt::struct-constructor) constructor))

;;-----------------------------------------------------------------------------
;; COM.INFORMATIMAGO.CLICC.RT::STRUCT-PRINTER symbol
;;-----------------------------------------------------------------------------
(defun com.informatimago.clicc.rt:struct-printer (symbol)
  (get symbol 'com.informatimago.clicc.rt::struct-printer))

;;-----------------------------------------------------------------------------
;; (SETF COM.INFORMATIMAGO.CLICC.RT::STRUCT-PRINTER) print-function symbol
;;-----------------------------------------------------------------------------
(defun (setf com.informatimago.clicc.rt:struct-printer) (print-function symbol)
  (setf (get symbol 'com.informatimago.clicc.rt::struct-printer) print-function))

