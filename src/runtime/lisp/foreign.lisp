;;;-----------------------------------------------------------------------------
;;; CLiCC: The Common Lisp to C Compiler
;;; Copyright (C) 1994 Wolfgang Goerigk, Ulrich Hoffmann, Heinz Knutzen 
;;; Christian-Albrechts-Universitaet zu Kiel, Germany
;;;-----------------------------------------------------------------------------
;;; CLiCC has been developed as part of the APPLY research project,
;;; funded by the German Ministry of Research and Technology.
;;; 
;;; CLiCC is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; CLiCC is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License in file COPYING for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;-----------------------------------------------------------------------------
;;; Funktion : Laufzeitfunktionen des FFI
;;;
;;; $Revision: 1.15 $
;;; $Id: foreign.lisp,v 1.15 1994/12/17 11:58:18 pm Exp $
;;------------------------------------------------------------------------------

(com.informatimago.clicc::in-package "COM.INFORMATIMAGO.CLICC.LISP")

;;------------------------------------------------------------------------------
;; Die C-Typen anlegen
;;------------------------------------------------------------------------------
(deftype com.informatimago.clicc.ffi:c-char ()  `(satisfies com.informatimago.clicc.ffi:c-char-p))
(deftype com.informatimago.clicc.ffi:c-short () `(satisfies com.informatimago.clicc.ffi:c-short-p))
(deftype com.informatimago.clicc.ffi:c-int ()   `(satisfies com.informatimago.clicc.ffi:c-int-p))
(deftype com.informatimago.clicc.ffi:c-long ()  `(satisfies com.informatimago.clicc.ffi:c-long-p))
(deftype com.informatimago.clicc.ffi:c-unsigned-char ()  `(satisfies com.informatimago.clicc.ffi:c-unsigned-char-p))
(deftype com.informatimago.clicc.ffi:c-unsigned-short () `(satisfies com.informatimago.clicc.ffi:c-unsigned-short-p))
(deftype com.informatimago.clicc.ffi:c-unsigned-int ()   `(satisfies com.informatimago.clicc.ffi:c-unsigned-int-p))
(deftype com.informatimago.clicc.ffi:c-unsigned-long ()  `(satisfies com.informatimago.clicc.ffi:c-unsigned-long-p))
(deftype com.informatimago.clicc.ffi:c-float ()        `(satisfies com.informatimago.clicc.ffi:c-float-p))
(deftype com.informatimago.clicc.ffi:c-double ()       `(satisfies com.informatimago.clicc.ffi:c-double-p))

(deftype com.informatimago.clicc.ffi:c-string () '(satisfies com.informatimago.clicc.ffi:c-string-p))

;;------------------------------------------------------------------------------
;; Konstanten
;;------------------------------------------------------------------------------
(defconstant NO-CHARACTER "The evaluated value ~S is not a character.")
(defconstant NO-INTEGER "The evaluated value ~S is not an integer.")
(defconstant NO-FLOAT "The evaluated value ~S is not an float.")
(defconstant NO-STRING "The evaluated value ~S is not a string")

;;------------------------------------------------------------------------------
;; Die Konvertierungsfunktionen-Funktionen zum "Casten" von C-Daten.
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:c-char (value)
  (cond 
    ((characterp value)                 ; Lisp-Character
     (com.informatimago.clicc.rt::make-c-char value))
    ((integerp value)                   ; Lisp-Integer
     (com.informatimago.clicc.rt::make-c-char-2 value))         
    ((com.informatimago.clicc.rt::c-primitive-p value)          ; c-char
     (com.informatimago.clicc.rt::cast-c-char value))
    (t (error-in "C-CHAR" NO-CHARACTER value))))

(defun com.informatimago.clicc.ffi:c-short (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-short value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-short value))
    (t (error-in "C-SHORT" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:c-int (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-int value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-int value))
    (t (error-in "C-INT" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:c-long (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-long value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-long value))
    (t (error-in "C-LONG" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:c-unsigned-char (value)
  (cond 
    ((characterp value)                 ; Lisp-Character
     (com.informatimago.clicc.rt::make-c-unsigned-char value))
    ((integerp value)                   ; Lisp-Integer
     (com.informatimago.clicc.rt::make-c-unsigned-char-2 value))         
    ((com.informatimago.clicc.rt::c-primitive-p value)          ; c-char
     (com.informatimago.clicc.rt::cast-c-unsigned-char value))
    (t (error-in "C-UNSIGNED-CHAR" NO-CHARACTER value))))

(defun com.informatimago.clicc.ffi:c-unsigned-short (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-unsigned-short value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-unsigned-short value))
    (t (error-in "C-UNSIGNED-SHORT" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:c-unsigned-int (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-unsigned-int value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-unsigned-int value))
    (t (error-in "C-UNSIGNED-INT" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:c-unsigned-long (value)
  (cond 
    ((integerp value)
     (com.informatimago.clicc.rt::make-c-unsigned-long value))
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::cast-c-unsigned-long value))
    (t (error-in "C-UNSIGNED-LONG" NO-INTEGER value))))

;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:c-float (value)
  (cond 
    ((floatp value)
     (com.informatimago.clicc.rt::make-c-float value))
    ((com.informatimago.clicc.rt::c-floating-p value)
     (com.informatimago.clicc.rt::cast-c-float value))
    (t (error-in "C-FLOAT" NO-FLOAT value))))

(defun com.informatimago.clicc.ffi:c-double (value)
  (cond 
    ((floatp value)
     (com.informatimago.clicc.rt::make-c-double value))
    ((com.informatimago.clicc.rt::c-floating-p value)
     (com.informatimago.clicc.rt::cast-c-double value))
    (t (error-in "C-DOUBLE" NO-FLOAT value))))

;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:make-c-string (value)
  (cond
    ((stringp value)
     (com.informatimago.clicc.rt::make-c-string value))
    ((com.informatimago.clicc.ffi:c-string-p value)
     value)
    (t (error-in "MAKE-C-STRING" NO-STRING value))))

;;------------------------------------------------------------------------------
;; Kopierfunktion fuer Zeichenketten
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:copy-c-string (value)
  (if (com.informatimago.clicc.ffi:c-string-p value)
      (com.informatimago.clicc.rt::copy-c-string value)
      (error-in "COPY-C-STRING" NO-STRING value)))

;;------------------------------------------------------------------------------
;; Konvertierungsfunktionen von C nach Lisp.
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:lisp-character (value)
  (cond
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::make-lisp-character value))
    ((characterp value)
     value)
    (t (error-in "LISP-CHARACTER" NO-CHARACTER value))))

(defun com.informatimago.clicc.ffi:lisp-integer (value)
  (cond 
    ((com.informatimago.clicc.rt::c-primitive-p value)
     (com.informatimago.clicc.rt::make-lisp-integer value))
    ((integerp value)
     value)
    (t (error-in "LISP-INTEGER" NO-INTEGER value))))

(defun com.informatimago.clicc.ffi:lisp-float (value)
  (cond
    ((com.informatimago.clicc.rt::c-floating-p value)
     (com.informatimago.clicc.rt::make-lisp-float value))
    ((floatp value)
     value)
    (t (error-in "LISP-FLOAT" NO-FLOAT value))))

(defun com.informatimago.clicc.ffi:make-lisp-string (value)
  (cond
    ((com.informatimago.clicc.ffi:c-string-p value)
     (com.informatimago.clicc.rt::make-lisp-string value))
    ((stringp value)
     value)
    (t (error-in "MAKE-LISP-STRING" NO-STRING value))))

;;------------------------------------------------------------------------------
;; Testfunktionen
;;------------------------------------------------------------------------------
(defun com.informatimago.clicc.ffi:c-char-p (object)
  (or (com.informatimago.clicc.rt::c-char-p object)
      (com.informatimago.clicc.rt::check-char object)))

(defun com.informatimago.clicc.ffi:c-short-p (object)
  (or (com.informatimago.clicc.rt::c-short-p object)
      (com.informatimago.clicc.rt::check-short object)))

(defun com.informatimago.clicc.ffi:c-int-p (object)
  (or (com.informatimago.clicc.rt::c-int-p object)
      (com.informatimago.clicc.rt::check-int object)))

(defun com.informatimago.clicc.ffi:c-long-p (object)
  (or (com.informatimago.clicc.rt::c-long-p object)
      (com.informatimago.clicc.rt::check-long object)))

(defun com.informatimago.clicc.ffi:c-unsigned-char-p (object)
  (or (com.informatimago.clicc.rt::c-unsigned-char-p object)
      (com.informatimago.clicc.rt::check-unsigned-char object)))

(defun com.informatimago.clicc.ffi:c-unsigned-short-p (object)
  (or (com.informatimago.clicc.rt::c-unsigned-short-p object)
      (com.informatimago.clicc.rt::check-unsigned-short object)))

(defun com.informatimago.clicc.ffi:c-unsigned-int-p (object)
  (or (com.informatimago.clicc.rt::c-unsigned-int-p object)
      (com.informatimago.clicc.rt::check-unsigned-int object)))

(defun com.informatimago.clicc.ffi:c-unsigned-long-p (object)
  (or (com.informatimago.clicc.rt::c-unsigned-long-p object)
      (com.informatimago.clicc.rt::check-unsigned-long object)))

(defun com.informatimago.clicc.ffi:c-float-p (object)
  (or (com.informatimago.clicc.rt::c-float-p object)
      (com.informatimago.clicc.rt::check-float object)))

(defun com.informatimago.clicc.ffi:c-double-p (object)
  (or (com.informatimago.clicc.rt::c-double-p object)
      (com.informatimago.clicc.rt::check-double object)))

;;------------------------------------------------------------------------------
;; 
;;------------------------------------------------------------------------------
